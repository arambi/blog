<?php
namespace Blog\Test\TestCase\Controller;

use Blog\Controller\EventsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Blog\Controller\EventsController Test Case
 */
class EventsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.events'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
