<?php
namespace Blog\Test\TestCase\Controller\Admin;

use Blog\Controller\Admin\AuthorsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Blog\Controller\Admin\AuthorsController Test Case
 */
class AuthorsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.authors'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
