<?php
namespace Blog\Test\TestCase\Controller\Admin;

use Blog\Controller\Admin\EventsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Blog\Controller\Admin\EventsController Test Case
 */
class EventsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.events'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
