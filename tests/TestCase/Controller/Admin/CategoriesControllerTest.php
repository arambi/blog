<?php
namespace Blog\Test\TestCase\Controller\Admin;

use Blog\Controller\Admin\CategoriesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Blog\Controller\Admin\CategoriesController Test Case
 */
class CategoriesControllerTest extends IntegrationTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'Categories' => 'plugin.blog.categories', 
		'Articles' => 'plugin.blog.articles', 
		'Articles_title_translation' => 'plugin.blog.articles_title_translation', 
		'i18n' => 'plugin.blog.i18n'
	];

/**
 * Test index method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test view method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test add method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test edit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test delete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('Not implemented yet.');
	}

}
