<?php
namespace Blog\Test\TestCase\Controller;

use Blog\Controller\EventDatesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Blog\Controller\EventDatesController Test Case
 */
class EventDatesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.event_dates'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
