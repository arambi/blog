<?php
namespace Blog\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Blog\Model\Table\TagsTable;
use Cake\TestSuite\TestCase;

/**
 * Blog\Model\Table\TagsTable Test Case
 */
class TagsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'Tags' => 'plugin.blog.tags', 
		'Sites' => 'plugin.blog.sites', 
		'Articles' => 'plugin.blog.articles', 
		'Contents' => 'plugin.blog.contents'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Tags') ? [] : ['className' => 'Blog\Model\Table\TagsTable'];

		$this->Tags = TableRegistry::get('Tags', $config);

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tags);

		parent::tearDown();
	}

/**
 * Test initialize method
 *
 * @return void
 */
	public function testInitialize() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test validationDefault method
 *
 * @return void
 */
	public function testValidationDefault() {
		$this->markTestIncomplete('Not implemented yet.');
	}

}
