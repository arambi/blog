<?php
namespace Blog\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Blog\Model\Table\PostsCategoriesTable;
use Cake\TestSuite\TestCase;

/**
 * Blog\Model\Table\PostsCategoriesTable Test Case
 */
class PostsCategoriesTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'PostsCategories' => 'plugin.blog.posts_categories', 
		'Contents' => 'plugin.blog.contents', 
		'Categories' => 'plugin.blog.categories', 
		'Sites' => 'plugin.blog.sites', 
		'Articles' => 'plugin.blog.articles'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('PostsCategories') ? [] : ['className' => 'Blog\Model\Table\PostsCategoriesTable'];

		$this->PostsCategories = TableRegistry::get('PostsCategories', $config);

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PostsCategories);

		parent::tearDown();
	}

/**
 * Test initialize method
 *
 * @return void
 */
	public function testInitialize() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test validationDefault method
 *
 * @return void
 */
	public function testValidationDefault() {
		$this->markTestIncomplete('Not implemented yet.');
	}

}
