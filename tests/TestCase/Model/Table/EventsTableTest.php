<?php
namespace Blog\Test\TestCase\Model\Table;

use Blog\Model\Table\EventsTable;
use Manager\TestSuite\CrudTestCase;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Blog\Model\Table\EventsTable Test Case
 */
class EventsTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Blog\Model\Table\EventsTable
     */
    public $Events;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.events'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Events') ? [] : ['className' => 'Blog\Model\Table\EventsTable'];
        $this->Events = TableRegistry::get('Events', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Events);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Events);
        $this->assertCrudDataIndex( 'index', $this->Events);
      }
}
