<?php
namespace Blog\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Blog\Model\Table\PostsTable;
use Cake\TestSuite\TestCase;
use Cake\Core\App;
use I18n\Lib\Lang;

/**
 * Blog\Model\Table\PostsTable Test Case
 */
class PostsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'Rows' => 'plugin.block.rows', 
		'Contents' => 'plugin.blog.posts', 
    'Relationships' => 'plugin.taxonomy.relationships',
    'Terms' => 'plugin.taxonomy.terms',
    'Languages' => 'plugin.i18n.languages',
    'Slugs' => 'plugin.slug.slugs',
    'Translates' => 'plugin.taxonomy.translates',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Posts') ? [] : ['className' => 'Blog\Model\Table\PostsTable'];

		$this->Posts = TableRegistry::get('Posts', $config);
		$this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Posts);

		parent::tearDown();
	}

	public function setLanguages()
	{
		Lang::set( $this->Languages->find()->all());
	}

	public function testSave()
	{
		$this->setLanguages();

		$data = [
		    "rows" => [[
	        "position" => 1,
	        "blocks" => [[
	            "subtype" => "text",
	            "position" => 1,
	            "spa" => [
	                "body" => null
	            ],
	            "baq" => [
	                "body" => null
	            ],
	        ]],
	        "content_id" => "",
	        "totalCols" => "0"
		    ]],
		    "spa" => [
		        "title" => "asdfasd",
		        "tags" => []
		    ],
		    "baq" => [
		        "title" => "",
		        "tags" => []
		    ],
		    "categories" => [],
		    "tags" => ""
		];

		$content = $this->Posts->getNewEntity( $data);
		// Comprueba que el guardado nos da false
		$saved = $this->Posts->saveContent( $content);
		$this->assertTrue( !$saved);
	}

	public function testCrudConfig()
	{
		$view = $this->Posts->crud->buildView( 'update');
		_d( $view);
	}
}
