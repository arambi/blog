<?php
namespace Blog\Test\TestCase\Model\Table;

use Blog\Model\Table\AuthorsTable;
use Manager\TestSuite\CrudTestCase;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Blog\Model\Table\AuthorsTable Test Case
 */
class AuthorsTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Blog\Model\Table\AuthorsTable
     */
    public $Authors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Authors') ? [] : ['className' => 'Blog\Model\Table\AuthorsTable'];
        $this->Authors = TableRegistry::get('Authors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Authors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Authors);
        $this->assertCrudDataIndex( 'index', $this->Authors);
      }
}
