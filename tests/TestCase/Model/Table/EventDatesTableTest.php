<?php
namespace Blog\Test\TestCase\Model\Table;

use Blog\Model\Table\EventDatesTable;
use Manager\TestSuite\CrudTestCase;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Blog\Model\Table\EventDatesTable Test Case
 */
class EventDatesTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Blog\Model\Table\EventDatesTable
     */
    public $EventDates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.blog.event_dates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EventDates') ? [] : ['className' => 'Blog\Model\Table\EventDatesTable'];
        $this->EventDates = TableRegistry::get('EventDates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EventDates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->EventDates);
        $this->assertCrudDataIndex( 'index', $this->EventDates);
      }
}
