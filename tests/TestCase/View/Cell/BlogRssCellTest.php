<?php
namespace Blog\Test\TestCase\View\Cell;

use Blog\View\Cell\BlogRssCell;
use Cake\TestSuite\TestCase;

/**
 * Blog\View\Cell\BlogRssCell Test Case
 */
class BlogRssCellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->request = $this->getMock('Cake\Network\Request');
        $this->response = $this->getMock('Cake\Network\Response');
        $this->BlogRss = new BlogRssCell($this->request, $this->response);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BlogRss);

        parent::tearDown();
    }

    /**
     * Test display method
     *
     * @return void
     */
    public function testDisplay()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
