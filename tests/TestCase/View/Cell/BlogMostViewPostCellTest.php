<?php
namespace Blog\Test\TestCase\View\Cell;

use Blog\View\Cell\BlogMostViewPostCell;
use Cake\TestSuite\TestCase;

/**
 * Blog\View\Cell\BlogMostViewPostCell Test Case
 */
class BlogMostViewPostCellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->request = $this->getMock('Cake\Network\Request');
        $this->response = $this->getMock('Cake\Network\Response');
        $this->BlogMostViewPost = new BlogMostViewPostCell($this->request, $this->response);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BlogMostViewPost);

        parent::tearDown();
    }

    /**
     * Test display method
     *
     * @return void
     */
    public function testDisplay()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
