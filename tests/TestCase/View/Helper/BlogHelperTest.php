<?php
namespace Blog\Test\TestCase\View\Helper;

use Blog\View\Helper\BlogHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * Blog\View\Helper\BlogHelper Test Case
 */
class BlogHelperTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Blog = new BlogHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Blog);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
