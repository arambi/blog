<?php

$config ['Access'] = [
  'posts' => [
    'name' => __d( 'admin', 'Noticias'),
    'options' => [
      'update' => [
        'name' => __d( 'admin', 'Editar'),
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'add',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'setblock',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'update',
          ]
        ],
      ]
    ]
  ]
];