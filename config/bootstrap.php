<?php

use Cake\Core\Configure;
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use Block\Lib\BlocksRegistry;
use User\Auth\Access;
use Blog\Event\BlogListener;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;

// Event Listeners
EventManager::instance()->attach( new BlogListener());


ActionCollection::set( 'post', [
  'label' => 'Entrada de blog',
  'plugin' => 'Blog',
  'controller' => 'Posts',
  'action' => 'index',
  'icon' => 'fa fa-newspaper-o',
  'actions' => [
    'search' => '/s',
    'view' => '/:slug',
    'tag' => '/tag/:slug',
    'category' => '/category/:slug',
    'author' => '/author/:slug',
  ],
  'sitemap' => [
    'model' => 'Blog.Posts',
  ],
  'actionLayouts' => [
    'index' => 'Listado',
    'view' => 'Vista',
    'category' => 'Categorías',
    'tag' => 'Tags',
  ],
  'sectionsCategories' => [
    'categories' => [
      'label' => __d( 'app', 'Categorías'),
      'type' => 'select',
      'options' => function(){
        return TableRegistry::get( 'Taxonomy.Categories')->find( 'tree');
      }
    ]
  ]
]);


// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add( [
  'name' => 'Noticias',
  'icon' => 'fa fa-newspaper-o',
  'key' => 'posts',
  'url' => false
  
]);

NavigationCollection::add( [
  'parent' => 'posts',
  'name' => 'Listado de noticias',
  'parentName' => 'Noticias',
  'plugin' => 'Blog',
  'controller' => 'Posts',
  'action' => 'index',
  'icon' => 'fa fa-newspaper-o',
]);

NavigationCollection::add( [
  'parent' => 'posts',
  'name' => 'Nueva noticia',
  'parentName' => 'Noticias',
  'plugin' => 'Blog',
  'controller' => 'Posts',
  'action' => 'create',
  'icon' => 'fa fa-newspaper-o',
]);


NavigationCollection::add( [
  'parent' => 'posts',
  'parentName' => 'Noticias',
  'name' => 'Autores',
  'plugin' => 'Blog',
  'controller' => 'Authors',
  'action' => 'index',
  'icon' => 'fa fa-newspaper-o',
]);



// Bloque Últimas noticias
BlocksRegistry::add( 'posts', [
    'key' => 'posts',
    'title' => __d( 'admin', 'Últimas noticias'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\PostsBlock',
    'cell' => 'Blog.Posts::display',
    'blockView' => 'Blog/blocks/posts'
]);

Access::add( 'posts', [
    'name' => __d( 'admin', 'Noticias'),
    'options' => [
      'update' => [
        'name' => __d( 'admin', 'Editar'),
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'posts',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'draft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'savedraft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'deletedraft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'previewredirect',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'field',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'autocomplete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'bulk',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'add',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'setblock',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Block',
            'controller' => 'blocks',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Blog',
            'controller' => 'categories',
            'action' => 'field',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'previewredirect',
          ],
        ],
      ]
    ]
  ]);





// Bloque Noticia mas leida
BlocksRegistry::add( 'blog_most_view_post', [
    'key' => 'blog_most_view_post',
    'title' => __d( 'admin', 'Noticia mas leida'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\BlogMostViewPostBlock',
    'cell' => 'Blog.BlogMostViewPost::display',
    'blockView' => 'Blog/blocks/blog_most_view_post'
]);





// Bloque Etiquetas de noticias
BlocksRegistry::add( 'blog_tags', [
    'key' => 'blog_tags',
    'title' => __d( 'admin', 'Etiquetas de noticias'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\BlogTagsBlock',
    'cell' => 'Blog.BlogTags::display',
    'blockView' => 'Blog/blocks/blog_tags'
]);





// Bloque Categorias de blog
BlocksRegistry::add( 'blog_categories', [
    'key' => 'blog_categories',
    'title' => __d( 'admin', 'Categorias de blog'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\BlogCategoriesBlock',
    'cell' => 'Blog.BlogCategories::display',
    'blockView' => 'Blog/blocks/blog_categories'
]);





// Bloque RSS
BlocksRegistry::add( 'blog_rss', [
    'key' => 'blog_rss',
    'title' => __d( 'admin', 'RSS'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\BlogRssBlock',
    'cell' => 'Blog.BlogRss::display',
    'blockView' => 'Blog/blocks/blog_rss'
]);




Access::add( 'events', [
  'name' => 'Eventos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'autocomplete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Events',
          'action' => 'bulk',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'EventDates',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'EventDates',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'EventDates',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'EventDates',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'EventDates',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Sections',
          'action' => 'previewredirect',
        ],
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Eventos',
  'parentName' => 'Eventos',
  'plugin' => 'Blog',
  'controller' => 'Events',
  'action' => 'index',
  'icon' => 'fa fa-calendar-plus-o',
]);



ActionCollection::set( 'events', [
  'label' => 'Eventos',
  'plugin' => 'Blog',
  'controller' => 'Events',
  'action' => 'index',
  'icon' => 'fa fa-calendar-plus-o',
  'actions' => [
    'past' => '/past',
    'search' => '/s',
    'view' => '/:slug',
    'tag' => '/tag/:slug',
    'category' => '/category/:slug',
    'author' => '/author/:slug',
  ],
  'sitemap' => [
    'model' => 'Blog.Events',
  ],
]);






// Bloque Eventos
BlocksRegistry::add( 'events', [
    'key' => 'events',
    'title' => __d( 'admin', 'Eventos'),
    'icon' => 'fa fa-calendar-plus-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsBlock',
    'cell' => 'Blog.Events::display',
    'blockView' => 'Blog/blocks/events'
]);




Access::add( 'authors', [
  'name' => 'Autores',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Blog',
          'controller' => 'Authors',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);






// Bloque Autores
BlocksRegistry::add( 'authors', [
    'key' => 'authors',
    'title' => __d( 'admin', 'Autores'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\AuthorsBlock',
    'cell' => 'Blog.Authors::display',
    'blockView' => 'Blog/blocks/authors'
]);





// Bloque Publicaciones por meses
BlocksRegistry::add( 'posts_months', [
    'key' => 'posts_months',
    'title' => __d( 'admin', 'Publicaciones por meses'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\PostsMonthsBlock',
    'cell' => 'Blog.PostsMonths::display',
    'blockView' => 'Blog/blocks/posts_months'
]);





// Bloque Categorias de Eventos
BlocksRegistry::add( 'events_categories', [
    'key' => 'events_categories',
    'title' => __d( 'admin', 'Categorias de Eventos'),
    'icon' => 'fa fa-calendar-plus-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsCategoriesBlock',
    'cell' => 'Blog.EventsCategories::display',
    'blockView' => 'Blog/blocks/events_categories'
]);





// Bloque Eventos por meses
BlocksRegistry::add( 'events_months', [
    'key' => 'events_months',
    'title' => __d( 'admin', 'Eventos por meses'),
    'icon' => 'fa fa-calendar-plus-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsMonthsBlock',
    'cell' => 'Blog.EventsMonths::display',
    'blockView' => 'Blog/blocks/events_months'
]);





// Bloque Etiquetas de Eventos
BlocksRegistry::add( 'events_tags', [
    'key' => 'events_tags',
    'title' => __d( 'admin', 'Etiquetas de Eventos'),
    'icon' => 'fa fa-calendar-plus-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsTagsBlock',
    'cell' => 'Blog.EventsTags::display',
    'blockView' => 'Blog/blocks/events_tags'
]);





// Bloque Eventos pasados
BlocksRegistry::add( 'events_pasts', [
    'key' => 'events_pasts',
    'title' => __d( 'admin', 'Eventos pasados'),
    'icon' => 'fa fa-newspaper-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsPastsBlock',
    'cell' => 'Blog.EventsPasts::display',
    'blockView' => 'Blog/blocks/events_pasts'
]);





// Bloque Calendario de eventos
BlocksRegistry::add( 'events_calendar', [
    'key' => 'events_calendar',
    'title' => __d( 'admin', 'Calendario de eventos'),
    'icon' => 'fa fa-calendar-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Blog\\Model\\Block\\EventsCalendarBlock',
    'cell' => 'Blog.EventsCalendar::display',
    'blockView' => 'Blog/blocks/events_calendar'
]);
