<?php

use Phinx\Migration\AbstractMigration;

class Events extends AbstractMigration
{
  public function up()
  {
    $event_dates = $this->table( 'event_dates');
    $event_dates
      ->addColumn( 'place', 'string', ['default' => null, 'null' => true])
      ->addColumn( 'event_id', 'integer', [ 'default' => null, 'null' => true])
      ->addColumn( 'type', 'string', ['limit' => 16, 'default' => null])
      ->addColumn( 'salt', 'string', ['limit' => 64, 'default' => null])
      ->addColumn( 'published_at', 'datetime', ['default' => null, 'null' => true])
      ->addColumn( 'start_on', 'date', ['default' => null, 'null' => true])
      ->addColumn( 'finish_on', 'date', ['default' => null, 'null' => true])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['event_id'])
      ->addIndex( ['type'])
      ->addIndex( ['salt'])
      ->addIndex( ['published_at'])
      ->addIndex( ['start_on'])
      ->addIndex( ['finish_on'])
      ->save();
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $this->dropTable( 'event_dates');
  }
}
