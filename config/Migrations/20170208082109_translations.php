<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function up()
  {
    $authors = $this->table( 'blog_authors_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $authors
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'blog_authors_translations');
  }
}
