<?php
use Migrations\AbstractMigration;

class EventDatesTranslations extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $event_dates_translations = $this->table( 'event_dates_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $event_dates_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'place', 'string', ['null' => true, 'default' => null])
      ->create(); 
  }
}
