<?php

use Phinx\Migration\AbstractMigration;

class Authors extends AbstractMigration
{

  public function change()
  {
    $authors = $this->table( 'blog_authors');
    $authors
        ->addColumn( 'name', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'email', 'string', ['null' => true, 'default' => null, 'limit' => 50])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'slug', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])
        ->addColumn( 'photo', 'text', ['default' => NULL, 'null' => true])
        ->addColumn( 'settings', 'text', ['default' => NULL, 'null' => true])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->save();
  }
}
