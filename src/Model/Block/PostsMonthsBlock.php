<?php

namespace Blog\Model\Block;

class PostsMonthsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Publicaciones por meses'),
          'plural' => __d( 'admin', 'Publicaciones por meses'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'settings.limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 10]
        ],
        'settings.show_number' => [
          'label' => __d( 'admin', 'Mostrar número'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Marca esta opción para que aparezca el número de publicaciones por mes')
        ],
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings.limit',
                  'settings.show_number',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}