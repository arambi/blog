<?php

namespace Blog\Model\Block;

class EventsCalendarBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Calendario de eventos'),
          'plural' => __d( 'admin', 'Calendario de eventos'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}