<?php

namespace Blog\Model\Block;

class EventsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Eventos'),
          'plural' => __d( 'admin', 'Eventos'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'settings.limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 10]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings.limit'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}