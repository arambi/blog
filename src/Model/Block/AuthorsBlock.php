<?php

namespace Blog\Model\Block;

class AuthorsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Autores'),
          'plural' => __d( 'admin', 'Autores'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'settings' => [
          'type' => 'multi',
          'fields' => [
            'limit' => [
              'label' => 'Límite',
              'type' => 'numeric',
              'range' => [1, 10]
            ],
          ]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}