<?php

namespace Blog\Model\Block;

class EventsCategoriesBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Categorias de eventos'),
          'plural' => __d( 'admin', 'Categorias de eventos'),
        ])
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Título'),
          'default' => __d( 'admin', 'Etiquetas')
        ],
        'key' => [
          'type' => 'hidden'
        ],
        'settings.limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 10]
        ]
      ]);


    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings.limit'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}