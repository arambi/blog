<?php

namespace Blog\Model\Block;

class BlogRssBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'RSS'),
          'plural' => __d( 'admin', 'RSS'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}