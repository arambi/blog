<?php

namespace Blog\Model\Block;

class EventsMonthsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Eventos por meses'),
          'plural' => __d( 'admin', 'Eventos por meses'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ]
        'settings' => [
          'type' => 'multi',
          'fields' => [
            'limit' => [
              'label' => 'Límite',
              'type' => 'numeric',
              'range' => [1, 10]
            ],
            'show_number' => [
              'label' => __d( 'admin', 'Mostrar número'),
              'type' => 'boolean',
              'help' => __d( 'admin', 'Marca esta opción para que aparezca el número de publicaciones por mes')
            ]
          ]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}