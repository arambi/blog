<?php

namespace Blog\Model\Block;

class EventsTagsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Etiquetas de eventos'),
          'plural' => __d( 'admin', 'Etiquetas de eventos'),
        ])
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Título'),
          'default' => __d( 'admin', 'Etiquetas')
        ],
        'key' => [
          'type' => 'hidden'
        ],
        'settings' => [
          'type' => 'multi',
          'fields' => [
            'limit' => [
              'label' => 'Límite',
              'type' => 'numeric',
              'range' => [1, 50]
            ]
          ]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}