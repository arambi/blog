<?php

namespace Blog\Model\Block;

use Website\Lib\Website;
use Cake\ORM\TableRegistry;

class PostsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Últimas noticias'),
          'plural' => __d( 'admin', 'Últimas noticias'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'settings.limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 10]
        ]
      ]);

    $elements = [
      'title',
      'settings.limit'
    ];

    if( Website::get( 'settings.blog_with_categories'))
    {
      $Table->crud->addFields([
        'settings.categories' => [
          'label' => __d( 'app', 'Solo de algunas categorías'),
          'type' => 'select',
          'options' => function( $crud){
            return TableRegistry::get( 'Taxonomy.Categories')->find( 'tree');
          },
          'template' => 'Manager.fields/select_multiple_ids'
        ]
      ]);

      $elements [] = 'settings.categories';
    }

    if( !Website::get( 'settings.blog_without_special'))
    {
      $Table->crud
        ->addFields([
          'settings.specials' => [
            'label' => __d( 'admin', 'Sólo destacadas'),
            'type' => 'boolean',
            'help' => __d( 'admin', 'Marca esta opción para que aparezcan sólo las noticias destacadas')
          ]
        ]);

      $elements [] = 'settings.specials';
    }

    

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'title' => __d( 'admin', 'General'),
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => $elements
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}