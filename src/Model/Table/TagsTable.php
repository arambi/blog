<?php
namespace Blog\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tags Model
 */
class TagsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('categories');
		$this->displayField('title');
		$this->primaryKey('id');
		$this->addBehavior('Timestamp');
		$this->belongsTo('Sites', [
			'alias' => 'Sites', 
			'foreignKey' => 'site_id', 
			'className' => 'Blog.Sites'
		]);
		$this->hasMany('Articles', [
			'alias' => 'Articles', 
			'foreignKey' => 'category_id', 
			'className' => 'Blog.Articles'
		]);
		$this->hasMany('Contents', [
			'alias' => 'Contents', 
			'foreignKey' => 'category_id', 
			'className' => 'Blog.Contents'
		]);
	}


/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->requirePresence('content_type', 'create')
			->notEmpty('content_type')
			->add('site_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('site_id')
			->requirePresence('slug', 'create')
			->notEmpty('slug')
			->requirePresence('title', 'create')
			->notEmpty('title')
			->allowEmpty('settings');

		return $validator;
	}

}
