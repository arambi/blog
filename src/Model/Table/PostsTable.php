<?php

namespace Blog\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Cache\Cache;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Cake\Validation\Validator;
use Cake\Datasource\EntityInterface;
use Cofree\Model\Traits\PostViewTrait;

/**
 * Posts Model
 */
class PostsTable extends Table
{
    use PostViewTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('contents');
        $this->displayField('title');
        $this->primaryKey('id');

        // Behaviors
        $this->addBehavior('Timestamp');
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Manager.Drafter');
        $this->addBehavior('Cofree.Publisher');
        $this->addBehavior('Block.Blockable', [
            'blocks' => [
                'text', 'gallery'
            ],
            'defaults' => [
                'text'
            ]
        ]);

        $this->addBehavior(Configure::read('I18n.behavior'), [
            'fields' => ['title', 'subtitle', 'summary', 'summary2'],
        ]);

        $this->addBehavior('Slug.Sluggable');
        $this->addBehavior('Search.Searchable', [
            'fields' => [
                'title',
                'subtitle',
                'summary',
                'rows.columns.blocks.body'
            ]
        ]);

        $this->buildTaxonomy();
        $this->buildAuthors();

        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.Contentable');

        $fields = ['photo'];

        if (Website::get('settings.blog_with_photos')) {
            $fields[] = 'photos';
        }

        if (Website::get('settings.blog_with_docs')) {
            $fields[] = 'docs';
        }

        if (Website::get('settings.blog_with_audios')) {
            $fields[] = 'audios';
        }

        $this->addBehavior('Upload.UploadJsonable', [
            'fields' => $fields
        ]);

        $this->addBehavior('Cofree.Jsonable', [
            'fields' => [
                'settings'
            ]
        ]);



        // CRUD Config
        $this->crud->associations(['Rows', 'Blocks']);

        $this->crud
            ->addFields([
                'published_at' => [
                    'label' => __d('admin', 'Fecha de publicación'),
                ],
                'hour' => [
                    'label' => 'Hora',
                    'type' => 'time',
                ],
                'published' => __d('admin', 'Publicado'),
                'content_link' => [
                    'type' => 'link',
                    'label' => 'Enlace',
                ],
                'title' =>  __d('admin', 'Título'),
                'subtitle' =>  __d('admin', 'Subtítulo'),
                'include_rss' =>  __d('admin', 'Incluir en RSS'),
                'summary' => [
                    'label' => __d('admin', 'Entradilla'),
                ],
                'summary2' => [
                    'label' => __d('admin', 'Texto de portada'),
                ],
                'special' => [
                    'type' => 'boolean',
                    'label' => __d('admin', 'Noticia destacada'),
                    'help' => __d('admin', 'Marca esta opción si quieres que las noticias estén en el bloque de últimas noticias'),
                ],
                'video' => [
                    'label' => __d('admin', 'Video (url)'),
                    'help' => __d('admin', 'Url del video de Youtube, Vimeo...')
                ],
                'photo' => [
                    'type' => 'upload',
                    'label' => __d('admin', 'Foto principal'),
                    'config' => [
                        'type' => 'post',
                        'size' => 'thm'
                    ]
                ],
            ]);

        if (Website::get('settings.blog_with_authors')) {
            $this->crud->addFields([
                'author' => [
                    'label' => __d('admin', 'Autor'),
                    'type' => 'BelongsTo'
                ]
            ]);
        }


        $this->crud->addIndex('index', [
            'fields' => $this->buildIndexElements(),
            'actionButtons' => ['create'],
            'saveButton' => false,
            'actionsBox' => [
                [
                    'method' => 'delete',
                    'title' => 'Borrar'
                ]
            ]
        ])
            ->setName([
                'singular' => __d('admin', 'Noticias'),
                'plural' => __d('admin', 'Noticias'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'key' => 'general',
                        'title' => __d('admin', 'Contenido'),
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => $this->buildElements()
                            ]
                        ],

                    ],

                ],
                'previewButton' => true,
                'actionButtons' => ['create', 'index']
            ], 'update')

            ->contentLink(true);


        $this->crud->order([
            'Posts.published_at' => 'desc'
        ]);

        // $this->crud->getView('index')
        //     ->set('exportCsv', true)
        //     ->set('csvFields', [
        //         'title' => 'Título',
        //         'body' => 'Texto'
        //     ]);

        $this->addBehavior('Seo.Seo');

        $this->crud->defaults([
            'include_rss' => true,
        ]);

        EventManager::instance()->dispatch(new Event('Model.Posts.afterInitialize', $this));
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (!empty($entity->hour) && !empty(strtotime($entity->published_at)) && strpos($entity->published_at, '-') !== false) {
            $entity->set('published_at', date('Y-m-d', strtotime($entity->published_at)) . ' ' . $entity->hour . ':00');
        }
    }

    public function afterSave(Event $event, EntityInterface $entity)
    {
        Cache::delete('Posts');
    }

    public function buildElements()
    {
        $elements = [
            'content_link',
            'title',
        ];

        if (!Website::get('settings.blog_without_subtitle')) {
            $elements[] = 'subtitle';
        }

        $elements = array_merge($elements, [
            'slugs',
            'published',
            'published_at',
            'hour'
        ]);

        if (!Website::get('settings.blog_without_special')) {
            $elements[] = 'special';
        }

        if (!Website::get('settings.blog_without_rss')) {
            $elements[] = 'include_rss';
        }

        if (!Website::get('settings.blog_without_video')) {
            $elements[] = 'video';
        }

        if (Website::get('settings.blog_with_authors')) {
            $elements[] = 'author';
        }

        if (Website::get('settings.blog_with_categories')) {
            $this->crud->addFields([
                'categories' => [
                    'type' => 'select_multiple',
                    'label' => __d('admin', 'Categorías'),
                    'addNew' => true
                ],
            ]);

            $elements[] = 'categories';
        }

        if (Website::get('settings.blog_with_tags')) {
            $this->crud->addFields([
                'tags' => [
                    'type' => 'tags',
                    'label' => __d('admin', 'Etiquetas'),
                ],
            ]);

            $elements[] = 'tags';
        }

        $elements[] = 'photo';

        if (Website::get('settings.blog_with_photos')) {
            $this->crud->addFields([
                'photos' => [
                    'type' => 'uploads',
                    'label' => __d('admin', 'Fotos'),
                    'config' => [
                        'type' => 'post',
                        'size' => 'thm'
                    ]
                ],
            ]);

            $elements[] = 'photos';
        }

        if (Website::get('settings.blog_with_docs')) {
            $this->crud->addFields([
                'docs' => [
                    'type' => 'uploads',
                    'label' => __d('admin', 'Archivos'),
                    'config' => [
                        'type' => 'doc',
                        'size' => 'thm'
                    ]
                ],
            ]);

            $elements[] = 'docs';
        }

        if (Website::get('settings.blog_with_audios')) {
            $this->crud->addFields([
                'audios' => [
                    'type' => 'uploads',
                    'label' => __d('admin', 'Audios'),
                    'config' => [
                        'type' => 'doc',
                    ]
                ]
            ]);

            $elements[] = 'audios';
        }

        $more = [
            'summary'
        ];

        if (!Website::get('settings.blog_without_summary2')) {
            $more[] = 'summary2';
        }

        $more[] = 'blocks';

        $elements = array_merge($elements, $more);

        return $elements;
    }


    public function buildIndexElements()
    {
        $elements = [
            'title',
            'published_at',
            'published',
        ];

        if (Website::get('settings.blog_with_authors')) {
            $elements[] = 'author';
        }

        if (!Website::get('settings.blog_without_special')) {
            $elements[] = 'special';
        }

        return $elements;
    }

    public function buildTaxonomy()
    {
        $models = [];

        if (Website::get('settings.blog_with_categories')) {
            $models['Categories'] = [
                'associationType' => 'belongsToMany',
            ];
        }

        if (Website::get('settings.blog_with_tags')) {
            $models['Tags'] = [
                'associationType' => 'belongsToMany',
                'type' => 'taggable'
            ];
        }

        if (!empty($models)) {
            $this->addBehavior('Taxonomy.Taxonomic', $models);
        }
    }


    public function buildAuthors()
    {
        $models = [];

        if (Website::get('settings.blog_with_authors')) {
            $this->belongsTo('Authors', [
                'className' => 'Blog.Authors',
                'foreignKey' => 'author_id'
            ]);

            $this->crud->addAssociations(['Authors']);
        }
    }

    /**
     * Contain para las categorias y tags
     * 
     * @param  Cake\ORM\Query  $query [description]
     */
    public function containTaxonomy(Query $query)
    {
        if (Website::get('settings.blog_with_categories')) {
            $query->contain(['Categories']);
        }

        if (Website::get('settings.blog_with_tags')) {
            $query->contain(['Tags']);
        }
    }

    public function findFrontend(Query $query)
    {
        if (Website::get('settings.blog_not_find_locales')) {
            $query->find('localeActives');
        }

        $query->order([
            'Posts.published_at' => 'desc',
            'Posts.hour' => 'desc',
            'Posts.created' => 'desc'
        ]);

        if (!Configure::read('Manager.preview')) {
            $query->where([
                'Posts.published' => 1,
                'Posts.published_at <= ' => date('Y-m-d')
            ]);
        }
        $this->containTaxonomy($query);

        if (Website::get('settings.blog_with_authors')) {
            $query->contain('Authors');
        }

        return $query;
    }

    /**
     * Búsqueda para noticias del frontend
     *
     * @param string 		$slug 		El slug, que será pasado en el caso de que se pida
     * @return Cake\ORM\Query
     */
    public function findFront($slug = null)
    {
        if ($slug !== null) {
            $query = $this->find('slug', [
                'slug' => $slug
            ]);
        } else {
            $query = $this->find();
        }

        $query->find('frontend');

        return $query;
    }

    /**
     * Retorna la URL de previsualización para la edición del contenido
     * 
     * @param  Entity $content
     * @return
     */
    public function getPreviewUrl($content)
    {
        return Router::url([
            'prefix' => false,
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'view',
            'slug' => $content->slug
        ], true);
    }

    /**
     * Anterior y posterior post
     * 
     * @return array
     */
    public function neighbours($current)
    {
        $previous = $this->findFront()
            ->where([
                'OR' => [
                    'Posts.published_at <' => $current->published_at->format('Y-m-d'),
                    'AND' => [
                        'Posts.published_at' => $current->published_at->format('Y-m-d'),
                        'Posts.created <' => $current->created->format('Y-m-d H:i:s'),
                    ],
                ],


                'Posts.id !=' => $current->id
            ])
            ->order(['Posts.published_at' => 'DESC', 'Posts.created' => 'DESC'])
            ->limit(2)
            ->toArray();

        $return['previous'] = array_key_exists(0, $previous) ? $previous[0] : false;
        $return['previous2'] = array_key_exists(1, $previous) ? $previous[1] : false;

        $next = $this->findFront()
            ->where([
                'OR' => [
                    'Posts.published_at >' => $current->published_at->format('Y-m-d'),
                    'AND' => [
                        'Posts.published_at' => $current->published_at->format('Y-m-d'),
                        'Posts.created >' => $current->created->format('Y-m-d H:i:s'),
                    ],
                ],


                'Posts.id !=' => $current->id
            ])
            ->order(['Posts.published_at' => 'ASC', 'Posts.created' => 'ASC'])
            ->first();

        $return['next'] = $next;

        return $return;
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator
        // 	->requirePresence( 'title')
        // 	->notEmpty( 'title');

        return $validator;
    }

    public function block($params = [])
    {
        $defaults = [
            'limit' => 5
        ];

        $params = array_merge($defaults, $params);

        $posts = $this->find('all')
            ->where([
                'Posts.published' => true,
                'Posts.published_at <= NOW()'
            ])
            ->limit($params['limit']);

        return $posts;
    }

    public function categoriesUseds($limit = 20)
    {
        $Categories = TableRegistry::get('Taxonomy.Categories');
        $stmt = $this->connection()->execute(
            "SELECT count(*) as total, taxonomy_terms.id 
			FROM `contents` 
			left join taxonomy_relationships on content_id = contents.id 
			left join taxonomy_terms on term_id = taxonomy_terms.id 
			WHERE contents.content_type = 'Posts' 
				AND taxonomy_terms.content_type = 'Categories' 
			GROUP BY taxonomy_terms.id 
			ORDER BY total desc
			LIMIT $limit"
        );

        $rows = $stmt->fetchAll('assoc');

        $ids = collection($rows)->extract('id')->toArray();


        $return = [];

        if (!empty($ids)) {
            $results = $Categories->find('list')
                ->where([
                    'Categories.id IN' => $ids
                ])
                ->toArray();;
    
            foreach ($ids as $id) {
                $return[$id] = $results[$id];
            }
        }

        return $return;
    }

    public function sitemap()
    {
        $query = $this->find('frontend');

        return [
            'query' => $query,
            'callback' => function ($content) {
                $url = \Cake\Routing\Router::url([
                    'plugin' => 'Blog',
                    'controller' => 'Posts',
                    'action' => 'view',
                    'slug' => $content->slug
                ], true);
                $date = date('Y-m-d', $content->modified->toUnixString());

                return [
                    'url' => $url,
                    'date' => $date
                ];
            }
        ];
    }
}
