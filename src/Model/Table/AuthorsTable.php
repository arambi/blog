<?php
namespace Blog\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Authors Model
 *
 * @method \Blog\Model\Entity\Author get($primaryKey, $options = [])
 * @method \Blog\Model\Entity\Author newEntity($data = null, array $options = [])
 * @method \Blog\Model\Entity\Author[] newEntities(array $data, array $options = [])
 * @method \Blog\Model\Entity\Author|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Blog\Model\Entity\Author patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Blog\Model\Entity\Author[] patchEntities($entities, array $data, array $options = [])
 * @method \Blog\Model\Entity\Author findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AuthorsTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('blog_authors');
    $this->displayField('name');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Sluggable', [
        'field' => 'name'
    ]);
    
    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo',
      ]
    ]);
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['body']
    ]);

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => [
        'settings'
      ]
    ]);
    
    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'name' => __d( 'admin', 'Nombre'),
        'email' => __d( 'admin', 'Email'),
        'body' => __d( 'admin', 'Biografía'),
        'slug' => __d( 'admin', 'Enlace'),
        'settings.blog' => [
          'label' => 'Blog',
          'type' => 'string'
        ],
        'settings.twitter' => [
          'label' => 'Twitter',
          'type' => 'string'
        ],
        'settings.facebook' => [
          'label' => 'Facebook',
          'type' => 'string'
        ],
        'settings.youtube' => [
          'label' => 'Youtube',
          'type' => 'string'
        ],
        'settings.linkedin' => [
          'label' => 'Linkedin',
          'type' => 'string'
        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'author',
            'size' => 'thm'
          ]
        ],
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'name',
          'email',
    
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Autores'),
        'plural' => __d( 'admin', 'Autores'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'name',
                  'slug',
                  'email',
                  'body',
                  'photo',
                ]
              ],
              [
                'title' => __d( 'admin', 'Enlaces'),
                'elements' => [
                  'settings.blog',
                  'settings.twitter',
                  'settings.facebook',
                  'settings.youtube',
                  'settings.linkedin',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
    
 }
}
