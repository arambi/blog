<?php
namespace Blog\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PostsCategories Model
 */
class PostsCategoriesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) 
	{
		$this->table( 'content_categories');
		$this->displayField( 'id');
		$this->primaryKey( 'id');
		$this->addBehavior( 'Timestamp');
		
		$this->belongsTo( 'Posts', [
			'foreignKey' => 'content_id', 
			'className' => 'Blog.Posts'
		]);
	}

}
