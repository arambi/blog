<?php
namespace Blog\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Blog\Model\Entity\Event;
use Cake\Event\EventManager;
use Cake\Validation\Validator;
use Cake\Event\Event as CakeEvent;
use Cake\Datasource\EntityInterface;
/**
 * Events Model
 */
class EventsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'contents');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    $this->hasMany( 'EventDates', [
      'className' => 'Blog.EventDates',
      'foreignKey' => 'event_id',
      'sort' => [
        'EventDates.published_at' => 'asc'
      ]
    ]);

    $this->addBehavior('Timestamp');
    
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => [
        'title',
        'subtitle',
        'antetitle',
        'summary',
        'summary2',
        'date_string'
      ],
    ]);
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Block.Blockable', [
      'blocks' => [
        'text', 'gallery'
      ],
      'defaults' => [
        'text'
      ]
    ]);

    $this->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
        'subtitle',
        'summary',
        'rows.columns.blocks.body'
      ]
    ]);

    $this->addBehavior( 'Slug.Sluggable');
    $this->addBehavior( 'Taxonomy.Taxonomic', [
      'Categories' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);

    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Contentable');
    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photos',
        'photo',
      ]
    ]);
    

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    $this->crud->associations(['EventDates']);

    $this->crud
      ->addFields([
        'antetitle' => __d( 'admin', 'Antetítulo'),
        'title' => __d( 'admin', 'Título'),
        'subtitle' => __d( 'admin', 'Subtítulo'),
        'published' => __d( 'admin', 'Publicado'),
        'event_dates' => [
          'label' => 'Días de celebración',
          'type' => 'hasMany'
        ],
        'summary' => __d( 'admin', 'Entradilla'),
        'summary2' => __d( 'admin', 'Texto de portada'),
        'content_link' => [
          'type' => 'link',
          'label' => 'Enlace',
        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
        'photos' => [
          'type' => 'uploads',
          'label' => __d( 'admin', 'Galería de fotos'),
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
        'categories' => [
          'type' => 'select_multiple',
          'label' => __d( 'admin', 'Categorías'),
        ],
        'tags' => [
          'type' => 'tags',
          'label' => __d( 'admin','Etiquetas'),
        ],
        'dont_show_dates' => [
          'type' => 'boolean',
          'label' => __d( 'admin', 'No mostrar fechas (solo texto)'),
        ],
        'has_date_string' => [
          'type' => 'boolean',
          'label' => __d( 'admin', 'Poner un texto para la fecha'),
        ],
        'date_string' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Texto para la fecha'),
          'show' => 'content.has_date_string'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'published',
          'published_at' => __d( 'admin', 'Fecha')
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
        'actionsBox' => [
					[
						'method' => 'delete',
						'title' => 'Borrar'
					]
				]
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Eventos'),
        'plural' => __d( 'admin', 'Eventos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'key' => 'general',
            'cols' => 8,
            'box' => [
              [
                'key' => 'general',
                'elements' => $this->buildElements()
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index'],
        'previewButton' => true,
      ], ['update'])
      ->order([
        'Events.published_at' => 'desc'
      ])
    ;
  
    $this->crud->contentLink( true);

  }

  public function getPreviewUrl( $content)
  {
    return Router::url([
    	'prefix' => false,
    	'plugin' => 'Blog',
    	'controller' => 'Events',
    	'action' => 'view',
    	'slug' => $content->slug
    ], true);
  }

  public function buildElements()
  {
    $elements = [
      'content_link',
      'antetitle',
      'title',
      'subtitle',
      'published',
      'dont_show_dates',
      'has_date_string',
      'date_string',
      'event_dates',
      'summary',
      'summary2',
      'slugs',
      'photos',
      'photo',
    ];

    if( Website::get( 'settings.events_with_categories'))
    {
      $this->crud->addFields([
        'categories' => [
          'type' => 'select_multiple',
          'label' => __d( 'admin', 'Categorías'),
        ],
      ]);

      $elements [] = 'categories';
    }

    if( Website::get( 'settings.events_with_tags'))
    {
      $this->crud->addFields([
        'tags' => [
          'type' => 'tags',
          'label' => __d( 'admin', 'Etiquetas'),
        ],
      ]);

      $elements [] = 'tags';
    }

    $elements [] = 'blocks';

    return $elements;
  }

  public function beforeSave( CakeEvent $event, EntityInterface $entity, ArrayObject $options)
  {
    $date = null;

    if( is_array( $entity->event_dates))
    {
      foreach( $entity->event_dates as $event_date)
      {
        if( $event_date->type == 'punctual')
        {
          if( is_object( $event_date->published_at))
          {
            $_date = $event_date->published_at->toUnixString();
          }
          else
          {
            $_date = strtotime( $event_date->published_at);
          }
        }
        else
        {
          if( is_object( $event_date->finish_on))
          {
            $_date = $event_date->finish_on->toUnixString();
          }
          else
          {
            $_date = strtotime( $event_date->finish_on);
          }
        }

        if( $date === null || $_date > $date)
        {
          $date = $_date;
        }
      }
    }

    if( $date)
    {
      $entity->set( 'published_at', date( 'Y-m-d', $date));
    }
  }

  public function buildTaxonomy()
  {
    $models = [];

    if( Website::get( 'settings.events_with_categories'))
    {
      $models ['Categories'] = [
        'associationType' => 'belongsToMany',
      ];
    }

    if( Website::get( 'settings.events_with_tags'))
    {
      $models ['Tags'] = [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ];
    }

    if( !empty( $models))
    {
      $this->addBehavior( 'Taxonomy.Taxonomic', $models);
    }
    
  }

/**
 * Contain para las categorias y tags
 * 
 * @param  Cake\ORM\Query  $query [description]
 */
  public function containTaxonomy( Query $query)
  {
    if( Website::get( 'settings.blog_with_categories'))
    {
      $query->contain( ['Categories']);
    }

    if( Website::get( 'settings.blog_with_tags'))
    {
      $query->contain( ['Tags']);
    }
  }

  public function findFront( Query $query, array $options)
  {
    if( !Configure::read( 'Manager.preview'))
    {
      $query
        ->where([
          'Events.published' => true,
        ]);
    }
    
    $query
      ->contain([
        'EventDates',
        'Categories',
        'Tags'
      ]);

    $this->containTaxonomy( $query);

    $this->fireEventBeforeFind( $query, $options);    
    return $query;
  }

/**
 * Query para los eventos publicados de futuro
 * 
 * @return Query
 */
  public function findFuture( $slug = null)
  {
    if( $slug !== null)
    {
      $query = $this->find( 'slug', [
        'slug' => $slug
      ]);
    }
    else
    {
      $query = $this->find();
    }

    $query->find( 'front')
      ->where([
        'Events.published_at >=' => date( 'Y-m-d')
      ])
      ->order([
        'Events.published_at' => 'asc'
      ]);

    return $query;
  }


/**
 * Query para los eventos publicados de futuro
 * 
 * @return Query
 */
  public function findPast( $slug = null)
  {
    if( $slug !== null)
    {
      $query = $this->find( 'slug', [
        'slug' => $slug
      ]);
    }
    else
    {
      $query = $this->find();
    }

    $query->find( 'front')
      ->order([
        'Events.published_at' => 'desc'
      ]);

    $query
      ->where([
        'Events.published_at <' => date( 'Y-m-d')
      ])
      ->order([
        'Events.published_at' => 'desc'
      ]);
    
    $this->fireEventBeforeFind( $query);
    return $query;
  }

  public function findMonth( Query $query, array $options = [])
  {
    $options = $options + ['grouping' => true];
    $this->hasOne( 'EventDates', [
      'className' => 'Blog.EventDates',
      'foreignKey' => 'event_id'
    ]);

    list( $year, $month) = explode( '-', $options ['month']);

    $a_date = "$year-$month-01";
    $daysOfMonth = date("t", strtotime( $a_date));

    $query
      ->find( 'front')
      ->contain( 'EventDates')
      ->where([
        'OR' => [
          function( $exp) use( $year, $month){
            return $exp->and_([
              'EventDates.type' => 'punctual',
              'YEAR(EventDates.published_at)' => $year,
              'MONTH(EventDates.published_at)' => $month,
            ]);
          },
          function( $exp) use( $year, $month, $daysOfMonth){
            return $exp->and_([
              'EventDates.type' => 'several',
              'OR' => [
                function( $exp) use( $year, $month, $daysOfMonth){
                  return $exp->and_([
                    'EventDates.start_on >=' => "$year/$month/01",
                    'EventDates.finish_on <=' => "$year/$month/$daysOfMonth",
                  ]);
                },
                function( $exp) use( $year, $month, $daysOfMonth){
                  return $exp->and_([
                    'EventDates.start_on <=' => "$year/$month/01",
                    'EventDates.finish_on >=' => "$year/$month/$daysOfMonth",
                  ]);
                },
                function( $exp) use( $year, $month, $daysOfMonth){
                  return $exp->and_([
                    'EventDates.start_on <=' => "$year/$month/01",
                    'EventDates.finish_on <=' => "$year/$month/$daysOfMonth",
                    'EventDates.finish_on >=' => "$year/$month/01",
                  ]);
                },
                function( $exp) use( $year, $month, $daysOfMonth){
                  return $exp->and_([
                    'EventDates.start_on >=' => "$year/$month/01",
                    'EventDates.start_on <=' => "$year/$month/$daysOfMonth",
                    'EventDates.finish_on >=' => "$year/$month/$daysOfMonth",
                  ]);
                }
              ]
            ]);
          }
        ]
      ]);
    
    if( $options ['grouping'])
    {
      $query->group([
        'Events.id'
      ]);
    }

    return $query; 
  }

  public function fireEventBeforeFind( $query, $options = [])
  {
    $event = new CakeEvent( 'Blog.Model.Events.beforeFind', $this, [
      $query,
      $options
    ]);

    EventManager::instance()->dispatch( $event);
  }


	public function sitemap()
  {
    $query = $this->find( 'front');

    return [
			'query' => $query,
			'callback' => function( $content){
				$url = \Cake\Routing\Router::url([
						'plugin' => 'Blog',
						'controller' => 'Events',
						'action' => 'view',
						'slug' => $content->slug
				], true);
				$date = date( 'Y-m-d', $content->modified->toUnixString());

				return [
					'url' => $url,
					'date' => $date
				];
			}
		];
  }

  
}
