<?php

namespace Blog\Model\Table;

use Blog\Model\Entity\EventDate;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * EventDates Model
 */
class EventDatesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('event_dates');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');

    // Behaviors
    $this->addBehavior('Manager.Crudable');
    $this->addBehavior('Cofree.Saltable');

    $this->addBehavior(Configure::read('I18n.behavior'), [
      'fields' => ['place'],
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'place' => [
          'label' => __d('admin', 'Lugar')
        ],
        'type' => [
          'label' => __d('admin', 'Tipo de evento'),
          'type' => 'select',
          'options' => [
            'punctual' => __d('admin', 'Puntual'),
            'several' => __d('admin', 'Tipo exposición')
          ]
        ],
        'published_at' => [
          'label' => __d('admin', 'Fecha de celebración'),
          'type' => 'datetime',
          'show' => 'content.type == "punctual"'
        ],
        'start_on' => [
          'label' => __d('admin', 'Comienzo'),
          'show' => 'content.type == "several"'
        ],
        'finish_on' => [
          'label' => __d('admin', 'Final'),
          'show' => 'content.type == "several"'
        ],
      ])
      ->addIndex('index', [
        'fields' => [
          'place',
          'type',
          'published_at',
          'start_on',
          'finish_on',

        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName([
        'singular' => __d('admin', 'Días de evento'),
        'plural' => __d('admin', 'Día de evento'),
      ])
      ->addView('create', [
        'columns' => [
          [
            'key' => 'general',
            'cols' => 8,
            'box' => [
              [
                'key' => 'general',
                'elements' => [
                  'place',
                  'type',
                  'published_at',
                  'start_on',
                  'finish_on',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => []
      ], ['update']);

    $this->crud->defaults([
      'type' => 'punctual',
    ]);
  }



  // public function validationDefault(Validator $validator) 
  // {
  //   $validator->add( 'start_on', 'custom', [
  //       'rule' => function( $value, $context) {
  //         $data = $context ['data'];

  //         if( $data ['type'] == 'several')
  //         {
  //           return $data ['start_on'] < $data ['finish_on'];
  //         }

  //         return true;
  //       },
  //       'message' => __d( 'admin', 'La fecha final tiene que ser mayor que la fecha de comienzo.')
  //     ]);

  //   return $validator;
  // }

}
