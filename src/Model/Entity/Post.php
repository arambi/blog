<?php
namespace Blog\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\Utility\Text;
use Blog\Model\Entity\BlogEntityTrait;
use I18n\Model\Entity\DatesEntityTrait;
use Search\Model\Entity\SearchEntityTrait;

/**
 * Post Entity.
 */
class Post extends Entity 
{
	use CrudEntityTrait;
  use TranslateTrait;
  use SlugTrait;
  use BlogEntityTrait;
  use DatesEntityTrait;
  use SearchEntityTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'*' => true,
		'content_type' => true,
		'site_id' => true,
		'parent_id' => true,
		'category_id' => true,
		'slug' => true,
		'published' => true,
		'published_at' => true,
		'title' => true,
		'antetitle' => true,
		'subtitle' => true,
		'settings' => true,
		'photo' => true,
		'photos' => true,
		'site' => true,
		'parent_post' => true,
		'category' => true,
		'content_categories' => true,
		'child_posts' => true,
		'sections' => true,
		'rows' => true,
		'categories' => true,
		'tags' => true,
		'slugs' => true,
		'summary2' => true,
		'summary' => true,
		'special' => true,
		'author' => true,
		'hour' => true,
		'products' => true,
	];

	protected $_virtual = [
		'hour'
	];

	protected function _getHour( $value = null)
	{
		if( !$value && is_object( $this->published_at))
		{
			return $this->published_at->format( 'H:i');
		}

		return $value;
	}
	

	public function imageIndex( $size = 'hor')
	{
		if( !empty( $this->photo) && is_object( $this->photo))
		{
			return $this->photo->image( $size);
		}
	}
}
