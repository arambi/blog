<?php
namespace Blog\Model\Entity;

use Cake\ORM\Entity;

/**
 * PostsCategory Entity.
 */
class PostsCategory extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'content_id' => true,
		'category_id' => true,
		'content' => true,
		'category' => true,
	];

}
