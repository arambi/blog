<?php

namespace Blog\Model\Entity;

use Cake\Cache\Cache;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\Utility\Text;


trait BlogEntityTrait
{

  public function convertAttrs(array $attrs)
  {
    $data = str_replace("=", '="', http_build_query($attrs, null, '" ', PHP_QUERY_RFC3986)) . '"';
    return $data;
  }

  protected function _getBody($value)
  {
    if ($value !== null) {
      return $value;
    }

    if (isset($this->rows[0]->columns[0]->blocks[0]) && $this->rows[0]->columns[0]->blocks[0]->subtype == 'text') {
      return $this->rows[0]->columns[0]->blocks[0]->body;
    }
  }

  protected function _getIndexSummary()
  {
    if (!empty($this->summary2)) {
      return $this->summary2;
    }

    if (!empty($this->summary)) {
      return $this->summary;
    }

    $body = $this->body;

    return Text::truncate(strip_tags($body), 200, [
      'exact' => false
    ]);
  }

  public function videoEmbed($attrs = [])
  {
    if (!$this->video) {
      return null;
    }

    $video_id = $this->videoId($this->video);

    if ($url = $this->youtube_embed_url) {
      return '<iframe ' . $this->convertAttrs($attrs) . ' src="' . $url . '" frameborder="0" allowfullscreen></iframe>';
    }
  }

  protected function _getYoutubeEmbedUrl()
  {
    $video_id = $this->videoId($this->video);

    if ($video_id) {
      return '//www.youtube.com/embed/' . $video_id . '?rel=0';
    }
  }

  public function videoId($string)
  {
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $string, $match)) {
      $video_id = $match[1];
      return $video_id;
    }
  }

  protected function _getYoutubeImage()
  {
    if (!empty($this->video)) {
      $string = $this->video;
    } else {
      $string = $this->body;
    }

    return $this->getYoutubeImage($string);
  }

  public function getYoutubeImage($url)
  {
    $video_id = $this->videoId($url);

    if ($video_id) {
      return 'https://i.ytimg.com/vi/' . $video_id . '/mqdefault.jpg';
    }
  }

  public function anyVideoEmbed($field)
  {
    $url = $this->$field;

    if (strpos($url, 'you') !== false && $video_id = $this->videoId($url)) {
      return '<iframe src="//www.youtube.com/embed/' . $video_id . '?rel=0" frameborder="0" allowfullscreen></iframe>';
    }

    if (strpos($url, 'vimeo') !== false) {
      $video_id = $this->getVimeoId($url);
      return '<iframe src="https://player.vimeo.com/video/' . $video_id . '" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
  }

  public function anyVideoEmbedAutoplay($field)
  {
    $url = $this->$field;

    if (strpos($url, 'you') !== false && $video_id = $this->videoId($url)) {
      return '<iframe src="//www.youtube.com/embed/' . $video_id . '?rel=0&autoplay=1&controls=0&&showinfo=0&loop=1&mute=1" frameborder="0" allowfullscreen></iframe>';
    }

    if (strpos($url, 'vimeo') !== false) {
      $video_id = $this->getVimeoId($url);
      return '<iframe src="https://player.vimeo.com/video/' . $video_id . '?title=0&byline=0&portrait=0&autoplay=1&loop=1&background=1" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
  }

  public function anyVideoEmbedUrl($field)
  {
    $url = $this->$field;

    if ($video_id = $this->videoId($url)) {
      return 'https://www.youtube.com/embed/' . $video_id . '?rel=0';
    }

    if (strpos($url, 'vimeo') !== false) {
      $video_id = $this->getVimeoId($url);
      return 'https://player.vimeo.com/video/' . $video_id;
    }
  }

  public function anyVideoImage($field)
  {
    $url = $this->$field;

    if ($video_id = $this->videoId($url)) {
      return $this->getYoutubeImage($url);
    }

    if (strpos($url, 'vimeo') !== false) {
      return $this->getVimeoImage($url);
    }
  }

  public function getVimeoImage($url)
  {
    $id = $this->getVimeoId($url);
    $cachekey = "vimeo$id";

    $data = Cache::read($cachekey);

    if (!$data) {
      $data = unserialize(file_get_contents("http://vimeo.com/api/v2/video/". $this->getVimeoId($url) .".php"));
      Cache::write($cachekey, $data);
    }

    if (is_array($data) && isset($data[0]['thumbnail_large'])) {
      return $data[0]['thumbnail_large'];
    }

  }


  public function getVimeoId($url)
  {
    $regs = array();

    $id = '';

    if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
      $id = $regs[3];
    }

    return $id;
  }

  public function videoProvider($field)
  {
    $url = $this->$field;

    if ($video_id = $this->videoId($url)) {
      return 'youtube';
    }

    if (strpos($url, 'vimeo') !== false) {
      return 'vimeo';
    }
  }
}
