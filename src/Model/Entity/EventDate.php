<?php
namespace Blog\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * EventDate Entity.
 *
 * @property int $id
 * @property string $place
 * @property int $event_id
 * @property string $type
 * @property string $salt
 * @property \Cake\I18n\Time $published_at
 * @property \Cake\I18n\Time $start_on
 * @property \Cake\I18n\Time $finish_on
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class EventDate extends Entity
{
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        // 'id' => false,
    ];
}
