<?php
namespace Blog\Model\Entity;

use Cake\Event\Event  as CakeEvent;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Blog\Model\Entity\BlogEntityTrait;
use I18n\Model\Entity\DatesEntityTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Slug\Model\Entity\SlugTrait;use Cake\ORM\Entity;

class Event extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;
  use SlugTrait;
  use BlogEntityTrait;
  use DatesEntityTrait;

  protected $_accessible = [
    '*' => true,
    'id' => false,
    'rows' => true,
    'slugs' => true,
  ];

  public function __construct(array $properties = [], array $options = [])
	{
		parent::__construct( $properties, $options);		
		$event = new CakeEvent( 'Block.Model.Entity.Event.construct', $this);
		EventManager::instance()->dispatch( $event);
	}

  public function firstDate()
  {
    $date = $this->event_dates [0];
    $method = 'date'. ucfirst( $date->type);
    $day = $this->$method( $date);
    $hour = $date->type == 'punctual' ? $date->published_at->format( 'H:i') : false;
    return [
      'date' => $day,
      'hour' => $hour,
    ];
  }

  public function dates()
  {
    $return = [];
    $punctuals = collection( $this->event_dates)->match( ['type' => 'punctual'])->toArray();
    $dates = $this->datesPunctual( $punctuals);
    $return = array_merge( $return, $dates);
    $permanents = collection( $this->event_dates)->match( ['type' => 'several'])->toArray();
    $dates = $this->datesSeveral( $permanents);
    $return = array_merge( $return, $dates);
    return $return;
  }

  public function hasPunctuals()
  {
    return !empty( collection( $this->event_dates)->match( ['type' => 'punctual'])->toArray());
  }

  public function datesSeveral( $dates)
  {
    $return = [];
    $groups = [];

    foreach( $dates as $date)
    {
      $groups [$date->place][] = $date;
    }

    foreach( $groups as $place => $_dates)
    {
      $dates = [];
      
      foreach( $_dates as $_date)
      {
        $one = strftime( Website::get( 'date_event_permanent'), $_date->start_on->toUnixString());
        $two = strftime( Website::get( 'date_event_permanent'), $_date->finish_on->toUnixString());
        $dates [] = [
          'date' => vsprintf( Website::get( 'date_event_permanent_expr'), [$one, $two]),
          'hour' => false
        ];
      }

      $return [] = [
        'dates' => $dates,
        'place' => $place
      ];
    }

    return $return;
  }

  public function datesPunctual( $dates)
  {
    $return = [];
    $groups = [];

    foreach( $dates as $date)
    {
      if (!empty($date->published_at)) {
        $groups [$date->place][$date->published_at->format( 'm') . $date->published_at->format( 'Hi')][] = $date;
      }
    }

    foreach( $groups as $place => $_dates)
    {      
      foreach( $_dates as $i => $__dates)
      {
        if( count( $__dates) == 1)
        {
          $return [] = [
            'dates' => [
              [
                'date' => strftime( Website::get( 'date_event_punctual'), $__dates[0]->published_at->toUnixString()),
                'hour' => current( $_dates)[0]->published_at->format( 'H:i')
              ]
            ],
            'place' => $place
          ];

          unset( $_dates [$i]);
        }
        else
        {
          foreach( $_dates as $i => $__dates2)
          {
            $days = [];

            foreach( $__dates2 as $key => $_date)
            {
              $days [] = $_date->published_at->format( 'd');
            }

            $format = str_replace( '%e', implode( ', ', $days), Website::get( 'date_event_punctual'));
            $date_places [] = [
              'date' => strftime( $format, $_date->published_at->toUnixString()),
              'hour' => $_date->published_at->format( 'H:i')
            ];
          }

          $return [] = [
            'dates' => $date_places,
            'place' => $place
          ];
        }
      }
    }

    return $return;
  }

  public function datePunctual( $date)
  {
    $timestamp = $date->published_at->toUnixString();
    return strftime(  Website::get( 'date_event_punctual'), $timestamp);
  }

  public function dateSeveral( $date)
  {
    $timestamp1 = $date->start_on->toUnixString();
    $timestamp2 = $date->finish_on->toUnixString();

    $one = strftime( Website::get( 'date_event_permanent'), $date->start_on->toUnixString());
    $two = strftime( Website::get( 'date_event_permanent'), $date->finish_on->toUnixString());
    return vsprintf( Website::get( 'date_event_permanent_expr'), [$one, $two]);
  }

  public function dateUnique()
  {
    $method = 'date'. ucfirst( $this->event_date->type);
    return $this->$method( $this->event_date);
  }

  public function hourUnique()
  {
    $hour = $this->event_date->type == 'punctual' ? $this->event_date->published_at->format( 'H:i') : false;
    return $hour;
  }
}
