<?php
namespace Blog\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tag Entity.
 */
class Tag extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'content_type' => true,
		'site_id' => true,
		'slug' => true,
		'title' => true,
		'settings' => true,
		'site' => true,
		'articles' => true,
		'contents' => true,
		'locale' => true
	];

}
