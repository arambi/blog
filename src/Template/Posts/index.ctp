<?php foreach( $contents as $content): ?>
    <div>
      <?= $this->Html->link( $content->title, [
        'plugin' => 'Blog',
        'controller' => 'Posts',
        'action' => 'view',
        'slug' => $content->slug,
        // 'section_id' => $this->request->params ['section']->id,
        // 'content_id' => $this->request->params ['section']->content_id
      ]) ?>
    </div>
<?php endforeach ?>