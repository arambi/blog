<?php $this->loadHelper( 'Rss') ?>
<?php $this->set( 'channel', [
  'title' => $this->Nav->pageTitle()
]) ?>
<?php 
  foreach ($contents as $article) 
  {
    $link = [
        'plugin' => 'Blog',
        'controller' => 'Posts',
        'action' => 'view',
        'slug' => $article->slug,
    ];

    $data = [
        'title' => $article->title,
        'link' => $link,
        'guid' => ['url' => $link, 'isPermaLink' => 'true'],
        'description' => $article->index_summary,
        'pubDate' => $article->published_at->toUnixString()
    ];

    if( !empty( $article->photo))
    {
      $data ['enclosure'] = [
        'url' => $article->photo->paths->hor,
        'type' => $article->photo->mime_type
      ];
    }
    echo  $this->Rss->item([], $data);
  }
?>

