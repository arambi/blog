<?php if( !empty( $block->title)): ?>
  <h3><?= $block->title ?></h3>
<?php endif ?>
<div class="event-calendar-wrap">
  <div id="event-calendar" class="event-calendar"></div>
  <div id="event-data" class="event-info"></div>

  <div class="event-calendar-legend">
    <?php foreach( \Cake\ORM\TableRegistry::get( 'Taxonomy.Categories')->find() as $category): ?>
      <span class="event-legend-<?= $category->slug ?>"><?= $category->title ?></span>
    <?php endforeach ?>
  </div>
</div>
<script id="event-template" type="text/template">
  {{#each events}}
  <div class="event-calendar-event">
    <div class="ece-date">{{ dates }}</div>
    <div class="ece-image">{{{ photo }}}</div>
    <div class="ece-title"><a href="{{ link }}">{{ title }}</a></div>
    <div class="ece-date-hour-place">
      {{#if hour}}
      <span class="ece-date-hour">{{ hour }}</span> 
      {{/if}}
      <span class="ece-date-place">{{ place }}</span>
    </div>
    <div class="ece-more-info">
      <a href="{{ link }}"><?= __d( 'app', 'Más información') ?> <i class="fa fa-caret-right"></i></a>
    </div>
  </div>
  {{/each}}
  <div class="ece-close"><i class="fa fa-remove"></i></div>
</script>
<script id="clndr-template" type="text/template">
  <div class="clndr-controls">
      <div class="clndr-previous-button"><i class="fa fa-caret-left"></i></div>
      <div class="month"><%= month %> <%= year %></div>
      <div class="clndr-next-button"><i class="fa fa-caret-right"></i></div>
  </div>
  <div class="clndr-grid">
      <div class="days-of-the-week">
      <% _.each(daysOfTheWeek, function (day) { %>
          <div class="header-day"><%= day %></div>
      <% }); %>
      </div>
      <div class="days">
        <% _.each(days, function (day) { %>
            <div class="<%= day.classes %> <% _.each(day.events, function (event) { %> <%= event.class %> <% }); %>"><%= day.day %></div>
        <% }); %>
      </div>
  </div>
</script>
<?php $this->Buffer->start() ?>
  <script type="text/javascript">
    $(function(){
      moment.locale( '<?= $this->Nav->currentLang( 'iso2') ?>');
      var eventTpl = Handlebars.compile( $("#event-template").html());
      $('body').delegate( '.ece-close', 'click', function(){
        $('.event-info').removeClass( 'active');
      });

      function getEvents( cal, month){
        $.ajax({
          url: '/' + $('html').attr( 'lang') + '/blog/events/month.json',
          data: {
            month: month.format( 'YYYY-MM')
          },
          success: function( data){
            var events = [];
            for( i in data.events){
              var ev = data.events [i];
              var classes = '';
              
              if( ev.categories && ev.categories.length) {
                for( i in ev.categories){
                  classes += ' cat-' + ev.categories[i].slug;
                }
              }

              var event = {
                title: ev.title,
                dates: ev.dates,
                hour: ev.hour ? ev.hour : false,
                link: ev.link,
                place: ev.event_date.place,
                photo: ev.photo ? '<img src="' + ev.photo.paths.hor + '">' : '',
                class: classes
              };

              if( ev.type == 'punctual'){
                event.startDate = ev.published_at;
                event.endDate = ev.published_at;
              } else {
                event.startDate = ev.start_on;
                event.endDate = ev.finish_on;
              }
              events.push( event);
            }

            cal.setEvents( events);
          }
        })
      }
      $('#event-calendar').clndr({
        template: $("#clndr-template").html(),
        multiDayEvents: {
            endDate: 'endDate',
            startDate: 'startDate',
        },

        daysOfTheWeek: [
          '<?= __d( 'app', 'Lun') ?>', 
          '<?= __d( 'app', 'Mar') ?>', 
          '<?= __d( 'app', 'Mie') ?>',
          '<?= __d( 'app', 'Jue') ?>',
          '<?= __d( 'app', 'Vie') ?>',
          '<?= __d( 'app', 'Sab') ?>',
          '<?= __d( 'app', 'Dom') ?>', 
          
        ],
        clickEvents: {
          click: function( target){
            if( target.events && target.events.length > 0) {
              $("#event-data").html( eventTpl( {events: target.events}));
              $('.event-info').addClass( 'active');
            }
          },
          onMonthChange: function( month){
            getEvents( this, month);
          }
        },
        weekOffset: 0,
        ready: function(){
          getEvents( this, this.month);
        }
      });
    })
  </script>
<?php $this->Buffer->end() ?>