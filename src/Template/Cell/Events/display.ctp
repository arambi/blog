<h3 class="title-style"><?= __d( 'app', 'Eventos') ?><span class="title-block"></span><span class="normal"><?= $block->title ?></span></h3>
<div class="text-slider">
  <ul class="slides">
    <li>
      <?php foreach( $contents as $key => $content): ?>
          <div class="one-half blog-event-one-half">
            <div class="event-preview">
              <?php if( $content->event_dates [0]->type == 'punctual'): ?>
                  <div class="event-date-wrapper">
                    <div class="event-month"><?= $content->firstDate( "%b") ?></div>
                    <div class="event-day"><?= $content->firstDate( "%e") ?></div>
                  </div>
              <?php endif ?>
                  
              <div class="event-entry-inner event-<?= $content->event_dates [0]->type ?>">
                <?php if( $content->event_dates [0]->type == 'several'): ?>
                  <h5><?= $content->firstDate( null, "%e/%m/%Y") ?></h5>
                <?php endif ?>

                <h4><?= $this->Html->link( $content->title, [
                      'plugin' => 'Blog',
                      'controller' => 'Events',
                      'action' => 'view',
                      'slug' => $content->slug,
                  ]) ?> <span><?= __d( 'app', 'Localización') ?>: <b><?= $content->event_dates [0]->place ?></b></span></h4>
                <p><?= $content->index_summary ?></p>
              </div>
            </div>
          </div>
          <?php if( ($key+1)%2 == 0): ?>
              </li><li>
          <?php endif ?>
      <?php endforeach ?>
    </li>
  </ul>
  <ul class="flex-direction-nav">
    <li><a class="flex-prev" href="#">Previous</a></li>
    <li><a class="flex-next" href="#">Next</a></li>
  </ul>
</div>