<h4><?= $block->title ?></h4>
<ul>
  <?php foreach( $months as $month): ?>
    <li><?= $this->Html->link( strftime( "%B %Y", strtotime( $month->year .'-'. $month->month . '-01')), [
        'plugin' => 'Blog',
        'controller' => 'Posts',
        'action' => 'index',
        '?' => [
          'month' => $month->month,
          'year' => $month->year,
        ]
    ])  ?></li>
  <?php endforeach ?>
</ul>