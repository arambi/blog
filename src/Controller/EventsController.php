<?php

namespace Blog\Controller;

use Cake\Event\Event;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Blog\Controller\AppController;
use Comment\Controller\CommentsTrait;
use Cake\Http\Exception\NotFoundException;

/**
 * Events Controller
 *
 * @property \Blog\Model\Table\EventsTable $Events
 */
class EventsController extends AppController
{
    use CommentsTrait;

    public function initialize()
    {
        parent::initialize();

        if (isset($this->Auth)) {
            $this->Auth->allow();
        }

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $query = $this->Events->findFuture();

        if (Website::get('settings.blog_not_find_locales')) {
            $query->find('localeActives');
        }

        // beforeFind Event
        $event = new Event('Blog.Controller.Event.index.beforeFind', $this, [
            $query
        ]);

        EventManager::instance()->dispatch($event);
        $this->Events->fireEventBeforeFind($query);
        $this->__index($query);
    }

    public function past()
    {
        $query = $this->Events->findPast();

        if (Website::get('settings.blog_not_find_locales')) {
            $query->find('localeActives');
        }

        $this->Events->fireEventBeforeFind($query);
        $this->__index($query);
    }

    private function __index($query)
    {
        if ($this->request->getQuery('year')) {
            $query
                ->where([
                    'YEAR(Events.published_at)' => $this->request->getQuery('year')
                ]);
        }

        if ($this->request->getQuery('month')) {
            $query
                ->where([
                    'MONTH(Events.published_at)' => $this->request->getQuery('month')
                ]);
        }

        $this->Events->fireEventBeforeFind($query);
        $this->set('contents', $this->paginate($query));
    }

    public function view()
    {
        $query = $this->Events
            ->find('slug', ['slug' => $this->request->getParam('slug')])
            ->find('front');

        if (Website::get('settings.blog_not_find_locales')) {
            $query->find('localeActives');
        }

        // beforeFind Event
        $this->Events->fireEventBeforeFind($query);
        $content = $query->first();

        if (!$content) {
            throw new NotFoundException(__('Página no encontrada'));
        }

        $this->Events->fireEventBeforeFind($query);
        $this->set(compact('content'));
    }

    public function category()
    {
        $content = $this->Events->Categories->find('slug', [
            'slug' => $this->request->getParam('slug')
        ])
            ->first();

        if (!$content) {
            throw new NotFoundException(__('Página no encontrada'));
        }

        $query = $this->Events->find()
            ->innerJoinWith('Categories', function ($q)  use ($content) {
                return $q->where(['Categories.id' => $content->id]);
            })
            ->order([
                'Events.published_at' => 'desc'
            ])
            ->contain([
                'EventDates',
                'Categories',
                'Tags',
            ]);

        $query->where([
            'Events.title !=' => ''
        ]);

        $this->Events->fireEventBeforeFind($query);

        $contents = $this->Paginator->paginate($query, $this->paginate);
        $category_name = $this->request->getParam('slug');
        $category = $content;
        $this->set(compact('contents', 'content', 'category', 'category_name'));
    }

    public function search()
    {
        $query = $this->Events->find('front')
            ->order([
                'Events.published_at' => 'desc'
            ]);
            
        if (Website::get('settings.blog_not_find_locales')) {
            $query->find('localeActives');
        }

        $query->find('fulltext', ['text' => $this->request->getQuery('q')]);
        $this->Events->fireEventBeforeFind($query);
        $this->set('contents', $this->Paginator->paginate($query, $this->paginate));
    }

    public function month()
    {
        $events = $this->Events
            ->find('month', [
                'month' => $this->request->getQuery('month'),
                'grouping' => false
            ])
            ->formatResults(function ($results) {
                return $results->map(function ($row) {
                    $row->set('dates', $row->dateUnique());

                    if ($row->event_date->type == 'punctual') {
                        $row->set('published_at', $row->event_date->published_at->format('Y-m-d'));
                    } else {
                        $row->set('start_on', $row->event_date->start_on->format('Y-m-d'));
                        $row->set('finish_on', $row->event_date->finish_on->format('Y-m-d'));
                    }

                    $row->set('type', $row->event_date->type);
                    $row->set('hour', $row->hourUnique());
                    $row->set('link', \Cake\Routing\Router::url([
                        'plugin' => 'Blog',
                        'controller' => 'Events',
                        'action' => 'view',
                        'slug' => $row->slug
                    ]));

                    $return = $row->extract([
                        'dates',
                        'title',
                        'type',
                        'photo',
                        'start_on',
                        'finish_on',
                        'event_date',
                        'published_at',
                        'hour',
                        'link',
                    ]);

                    if (!empty($row->categories)) {
                        foreach ($row->categories as $category) {
                            $return['categories'][] = $category->extract([
                                'title',
                                'slug'
                            ]);
                        }
                    }

                    return $return;
                });
            });

        if ($this->Events->associations()->has('Categories')) {
            $events->contain('Categories');
        }

        $this->Events->fireEventBeforeFind($events);

        $this->set(compact('events'));
        $this->set('_serialize', true);
    }
}
