<?php
namespace Blog\Controller;

use Blog\Controller\AppController;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Network\Exception\NotFoundException;
use Website\Lib\Website;
use Comment\Controller\CommentsTrait;
use Blog\Controller\PostsControllerTrait;

/**
 * Posts Controller
 *
 * @property Blog\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{
  use CommentsTrait;
  use PostsControllerTrait;

  public $helpers = ['Blog.Blog', 'Paginator'];
/**
 * Initialize method
 */
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }

    $this->loadComponent( 'Paginator');
  }



}
