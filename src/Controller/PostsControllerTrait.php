<?php
namespace Blog\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Comment\Controller\CommentsTrait;
use Cake\Http\Exception\NotFoundException;

trait PostsControllerTrait
{
 

  /**
   * Index method
   *
   * @return void
   */
  public function index()
  {
    $this->Posts->removeBehavior('Blockable');
    $query = $this->Posts->findFront();

    if( $this->request->query( 'year'))
    {
      $query
        ->where([
          'YEAR(Posts.published_at)' => $this->request->query( 'year')
        ]);
    }

    if( $this->request->query( 'month'))
    {
      $query
        ->where([
          'MONTH(Posts.published_at)' => $this->request->query( 'month')
        ]);
    }
    
    // Categories Section
    $section = $this->request->param( 'section');

    if( !empty( $section->settings->sections_categories_bool) && !empty( $section->settings->sections_categories->categories))
    {
      $query->innerJoinWith( 'Categories', function( $q)  use ($section){
          return $q->where(['Categories.id IN' => $section->settings->sections_categories->categories]);
      });
    }
    
    // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.index.beforeFind', $this, [
      $query
    ]);

    EventManager::instance()->dispatch( $event);

    $this->paginate = [
      'limit' => Website::get( 'settings.blog_posts_paginate_limit') ? Website::get( 'settings.blog_posts_paginate_limit') : 10
    ];


    if( $this->RequestHandler->isRss())
    {
      $query->where([
        'Posts.include_rss' => true
      ]);
    }

    $contents = $this->Paginator->paginate( $query, $this->paginate);

    $this->set( 'contents', $contents);
    $this->set( '_serialize', true);
  }

  public function search()
  {
    $query = $this->Posts->findFront();

    // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.index.beforeFind', $this, [
      $query
    ]);

    EventManager::instance()->dispatch( $event);

    if( $this->request->getQuery( 'q'))
    {
      $query->find( 'fulltext', ['text' => $this->request->query('q')]);
    }

    if( $this->request->getQuery( 'from'))
    {
      $query->where([
        'Posts.published_at >=' => date( 'Y-m-d', strtotime( $this->request->getQuery( 'from')))
      ]);
    }

    if( $this->request->getQuery( 'to'))
    {
      $query->where([
        'Posts.published_at <=' => date( 'Y-m-d', strtotime( $this->request->getQuery( 'to')))
      ]);
    }


    $this->paginate = [
      'limit' => Website::get( 'settings.blog_posts_paginate_limit') ? Website::get( 'settings.blog_posts_paginate_limit') : 10
    ];

   

    $this->set( 'contents', $this->Paginator->paginate( $query, $this->paginate));
  }

  public function categorized()
  {
    $section = $this->request->param( 'section');
    $links = [];

    foreach( \I18n\Lib\Lang::iso3() as $locale => $name)
    {
      if( $locale != \I18n\Lib\Lang::current( 'iso3'))
      {
        $url = Router::url([
          'locale' => $locale,
          'plugin' => $this->request->param( 'plugin'),
          'action' => $this->request->param( 'action'),
          'section_categories' => $this->request->param( 'section_categories'),
        ]);
        
        $url = str_replace( '/'. \I18n\Lib\Lang::current( 'iso2') .'/', '/'. \I18n\Lib\Lang::getIso2( $locale) .'/', $url);

        $links [\I18n\Lib\Lang::getIso2( $locale)] = [
          'name' => $name,
          'link' => $url
        ];
      }
      else
      {
        $links [\I18n\Lib\Lang::getIso2( $locale)] = [
          'name' => $name,
          'link' => false
        ];
      }
    }

    \Cake\Core\Configure::write( 'I18n.links', $links);

    $this->index();
    $this->render( 'index');
  }

  /**
  * View method
  *
  * @param string $idx
  * @return void
  */
  public function view()
  {
    $query = $this->Posts->findFront( $this->request->param( 'slug'));

    // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.view.beforeFind', $this, [
      $query
    ]);

    EventManager::instance()->dispatch( $event);

    $content = $query->first();

    if( !$content)
    {
      throw new NotFoundException( __( 'Página no encontrada'));
    }

    $neighbours = $this->Posts->neighbours( $content);
    $this->set( compact( 'content', 'neighbours'));
    Configure::write('BlockPosts.presentIds', [$content->id]);
    $this->Posts->addView( $content);
  }

  public function category()
  {
    $content = $this->Posts->Categories->find( 'slug', [
        'slug' => $this->request->param( 'slug')
      ])
      ->first();

    if( !$content)
    {
      throw new NotFoundException( __( 'Página no encontrada'));
    }

    $query = $this->Posts->findFront()
      ->innerJoinWith( 'Categories', function( $q)  use ($content){
          return $q->where(['Categories.id' => $content->id]);
      })
      ->contain([
        'Categories',
      ]);

    if( Website::get( 'settings.blog_with_tags'))
    {
      $query->contain( 'Tags');
    }
    
    $query->where([
      'Posts.title !=' => ''
    ]);

    $query->order([
      'Posts.published_at' => 'desc'
    ]);
    
    // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.index.beforeFind', $this, [
      $query
    ]);
    
    $contents = $this->Paginator->paginate( $query, $this->paginate);
    $category_name = $this->request->param( 'slug' );
    $category = $content;
    $this->set( compact( 'contents', 'content', 'category', 'category_name'));
  }

  public function author()
  {
    $author = $this->Posts->Authors->find()
      ->where([
        'Authors.slug' => $this->request->param( 'slug')
    ])->first();

    if( !$author)
    {
      $this->Section->notFound();
    }

    $query = $this->Posts->findFront();
    $query->where([
      'Posts.author_id' => $author->id
    ]);

    $this->paginate = [
      'limit' => Website::get( 'settings.blog_posts_paginate_limit') ? Website::get( 'settings.blog_posts_paginate_limit') : 10
    ];

    $this->set( compact( 'author'));
    $this->set( 'contents', $this->Paginator->paginate( $query, $this->paginate));
  }

  public function tag()
  {
    $tag = $this->Posts->Tags->find()
      ->where(['Tags.slug' => $this->request->params ['slug']])
      ->first();

    $query = $this->Posts->findFront()
      ->innerJoinWith( 'Tags', function( $q)  use ($tag){
          return $q->where(['Tags.id' => $tag->id]);
      });

    // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.index.beforeFind', $this, [
      $query
    ]);

    $contents = $this->paginate( $query);

    $this->set( compact( 'tag', 'contents'));
  }
}