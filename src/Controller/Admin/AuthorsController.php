<?php
namespace Blog\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Blog\Controller\AppController;

/**
 * Authors Controller
 *
 * @property \Blog\Model\Table\AuthorsTable $Authors
 */
class AuthorsController extends AppController
{
    use CrudControllerTrait;
}
