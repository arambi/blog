<?php
namespace Blog\Controller\Admin;

use Blog\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Posts Controller
 *
 * @property Blog\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController 
{
	use CrudControllerTrait;
  
  protected function _index( $query)
  {
    $query->group([
      'Posts.id'
    ]);
  }

	public function autocomplete()
  {
    $contents = $this->Table->find( 'fulltext', [
			'text' => $this->request->query ['q'],
			'all' => true
    ])
    ->all();

    $this->CrudTool->addSerialized([
      'results' => $contents->toArray()
    ]);
  }

}
