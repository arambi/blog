<?php
namespace Blog\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Blog\Controller\AppController;

/**
 * Events Controller
 *
 * @property \Blog\Model\Table\EventsTable $Events
 */
class EventsController extends AppController
{
    use CrudControllerTrait;
}
