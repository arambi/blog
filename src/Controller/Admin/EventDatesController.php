<?php
namespace Blog\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Blog\Controller\AppController;

/**
 * EventDates Controller
 *
 * @property \Blog\Model\Table\EventDatesTable $EventDates
 */
class EventDatesController extends AppController
{
    use CrudControllerTrait;
}
