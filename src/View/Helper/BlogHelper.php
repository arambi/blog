<?php

namespace Blog\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

/**
 * Blog helper
 */
class BlogHelper extends helper
{

    public $helpers = ['Upload.Upload', 'Html'];

    public function tags($tags = null, $params = [], $html_tag = 'li')
    {
        if ($tags === null) {
            $tags = TableRegistry::get('Taxonomy.Tags')->find()
                ->order(['Tags.title']);
        }
        $_params = [
            'plugin' => 'Blog',
            'controller' => 'Posts',
            'action' => 'tag',
        ];

        $params = $params + $_params;

        $out = [];

        foreach ($tags as $tag) {
            $link = $this->Html->link($tag->title, $params + ['slug' => $tag->slug]);

            $out[] = $this->Html->tag($html_tag, $link);
        }

        return implode("\n", $out);
    }

    public function categories(array $options = [])
    {
        $_defaults = [
            'model' => false,
            'limit' => 10
        ];

        $options = array_merge($_defaults, $options);

        if ($options['model'] && strpos($options['model'], '.') === false) {
            $options['model'] = 'Blog.' . $options['model'];
        }

        $Categories = TableRegistry::get('Taxonomy.Categories');

        if ($options['model']) {
            list($plugin, $model) = pluginSplit($options['model']);

            $Categories->belongsToMany($options['model'], [
                'className' => $options['model'],
                'foreignKey' => 'term_id',
                'targetForeignKey' => 'content_id',
                'joinTable' => 'taxonomy_relationships',
            ]);

            $categories = $Categories->find();

            $categories 
                ->select([
                    'total' => $categories->func()->count("$model.id")
                ])
                ->enableAutoFields(true)
                ->innerJoinWith($model, function ($q) use ($options, $model) {
                    return $q->where([$model . '.content_type' =>  $model]);
                })
                ->order([
                    'Categories.position'
                ])
                ->group([
                    'Categories.id'
                ]);
        } else {
            $categories = $Categories->find()
                ->order(['Categories.position']);
        }

        $categories->limit($options['limit']);

        return $categories;
    }


    public function categoriesList($categories = null, array $options = [])
    {
        $_defaults = [
            'controller' => 'Posts',
            'action' => 'category',
            'tag' => 'li',
            'model' => false,
            'limit' => 10
        ];

        $options = array_merge($_defaults, $options);

        $params = [
            'plugin' => 'Blog',
            'controller' => $options['controller'],
            'action' => $options['action'],
        ];

        $out = [];

        if ($categories === null) {
            $Categories = TableRegistry::get('Taxonomy.Categories');
            if ($options['model']) {
                $Categories->belongsToMany($options['model'], [
                    'className' => 'Blog.' .  $options['model'],
                    'foreignKey' => 'term_id',
                    'targetForeignKey' => 'content_id',
                    'joinTable' => 'taxonomy_relationships',
                ]);

                $categories = $Categories->find('all')
                    ->innerJoinWith($options['model'], function ($q) use ($options) {
                        return $q->where([$options['model'] . '.content_type' =>  $options['model']]);
                    })
                    ->group([
                        'Categories.id'
                    ]);
            } else {
                $categories = $Categories->find()
                    ->order(['Categories.title']);
            }

            $categories->limit($options['limit']);
        }

        foreach ($categories as $category) {
            if (!is_object($category)) {
                continue;
            }

            $link = $this->Html->link($category->title, $params + ['slug' => $category->slug]);
            $out[] = $this->Html->tag($options['tag'], $link, [
                'class' => $this->request->param('action') == 'category' && $this->request->param('slug') == $category->slug ? 'current' : false
            ]);
        }

        return implode("\n", $out);
    }

    public function years()
    {
        $years = TableRegistry::get('Blog.Posts')->find()
            ->group(['YEAR(Posts.published_at)'])
            ->select(['year' => 'YEAR(Posts.published_at)'])
            ->order(['Posts.published_at' => 'desc']);

        return $years;
    }
}
