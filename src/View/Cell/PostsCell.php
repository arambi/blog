<?php
namespace Blog\View\Cell;

use Cake\View\Cell;
use Website\Lib\Website;
use Cake\Core\Configure;
use Cake\Cache\Cache;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * Posts cell
 */
class PostsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $limit = !empty($block->settings->limit) ? $block->settings->limit  : 10;
    $this->loadModel( 'Blog.Posts');
    $posts = $this->Posts->findFront()
      ->limit( $limit)
      ->order([
        'Posts.published_at' => 'desc'
      ]);

    // Evita repetición de noticias en la misma página
    $present_ids = $this->presentIds();

    if( !empty( $present_ids))
    {
      $posts->where( function( $exp){
        return $exp->not([
          'Posts.id IN' => $this->presentIds()
        ]);
      });
    }

    if( !Website::get( 'settings.blog_without_special') && !empty( $block->settings->specials))
    {
      $posts->where([
        'Posts.special' => true
      ]);
    }

    $posts->where([
      'Posts.title !=' => ''
    ]);
    
    if( property_exists( $block->settings, 'categories') && !empty( $block->settings->categories))
    {
      $posts->innerJoinWith( 'Categories', function( $q)  use ($block){
          return $q->where(['Categories.id IN' => $block->settings->categories]);
      });
    }


    if( Website::get( 'settings.blog_with_categories'))
    {
      $posts->contain(['Categories']);
    }

    if( Website::get( 'settings.blog_with_tags'))
    {
      $posts->contain(['Tags']);
    }

      // beforeFind Event
    $event = new Event( 'Blog.Controller.Posts.index.beforeFind', $this, [
      $posts
    ]);

    EventManager::instance()->dispatch( $event);

    $this->writePresentIds( $posts->extract( 'id')->toArray());

      
    $this->set( compact( 'posts', 'block'));
  }
  
  private function writePresentIds( $ids)
  {
    $exists_ids = $this->presentIds();
    $ids = array_merge( $exists_ids, $ids);
    Configure::write( 'BlockPosts.presentIds', $ids);
  }

  private function presentIds()
  {
    return (array)Configure::read( 'BlockPosts.presentIds');
  }
}
