<?php
namespace Blog\View\Cell;

use Cake\View\Cell;

/**
 * EventsPasts cell
 */
class EventsPastsCell extends Cell
{

  public function display( $block)
  {
    $this->loadModel( 'Blog.Events');

    $contents = $this->Events->findPast()
      ->limit( $block->settings->limit)
      ->all();

    $this->set( compact( 'contents', 'block'));
  }
}
