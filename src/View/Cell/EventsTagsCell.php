<?php
namespace Blog\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * BlogTags cell
 */
class EventsTagsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  { 
    $Tags = TableRegistry::get( 'Taxonomy.Tags');
    $Tags->belongsToMany( 'Events', [
      'className' => 'Blog.Events',
      'foreignKey' => 'term_id',
      'targetForeignKey' => 'content_id',
      'joinTable' => 'taxonomy_relationships',
    ]);

    $tags = $Tags->find( 'all')
      ->innerJoinWith( 'Posts', function( $q) {
          return $q->where([
            'Events.content_type' => 'Events',
            'Events.published' => true,
            'Events.published_at <= NOW()'
          ]);
      })
      ->limit( $block->settings->limit);

    $this->set( compact( 'tags', 'block'));
  }
}
