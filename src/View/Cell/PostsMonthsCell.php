<?php
namespace Blog\View\Cell;

use Cake\View\Cell;

/**
 * PostsMonths cell
 */
class PostsMonthsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $this->loadModel( 'Blog.Posts');
    $months = $this->Posts->find()
      ->group([
          'YEAR(Posts.published_at)',
          'MONTH(Posts.published_at)',
      ])
      ->select( ['year' => 'YEAR(Posts.published_at)'])
      ->select( ['month' => 'MONTH(Posts.published_at)'])
      ->limit( $block->settings->limit)
      ->order(['Posts.published_at' => 'desc']);

    $this->set( compact( 'block', 'months'));
  }
}
