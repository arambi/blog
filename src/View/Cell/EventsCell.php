<?php
namespace Blog\View\Cell;

use Cake\View\Cell;

/**
 * Events cell
 */
class EventsCell extends Cell
{

  public function display( $block)
  {
    $this->loadModel( 'Blog.Events');

    $contents = $this->Events->findFuture()
      ->limit( $block->settings->limit)
      ->all();

    $this->set( compact( 'contents', 'block'));
  }
}
