<?php
namespace Blog\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * BlogMostViewPost cell
 */
class BlogMostViewPostCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $posts = TableRegistry::get( 'Blog.Posts')->findFront()
      ->where( 'Posts.published_at >= DATE(NOW()) - INTERVAL 7 DAY')
      ->order( [
        'Posts.view_count' => 'desc'
      ], true)
      ->limit( $block->settings->limit);

    $this->set( compact( 'posts', 'block'));
  }
}
