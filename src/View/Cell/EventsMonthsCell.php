<?php
namespace Blog\View\Cell;

use Cake\View\Cell;

/**
 * EventsMonths cell
 */
class EventsMonthsCell extends Cell
{

  public function display( $block)
  {
    $this->loadModel( 'Blog.Events');
    $months = $this->Events->find()
      ->group([
          'YEAR(Events.published_at)',
          'MONTH(Events.published_at)',
      ])
      ->select( ['year' => 'YEAR(Events.published_at)'])
      ->select( ['month' => 'MONTH(Events.published_at)'])
      ->limit( $block->settings->limit)
      ->order(['Events.published_at' => 'desc']);

    $this->set( compact( 'block', 'months'));
  }
}
