<?php
namespace Blog\View\Cell;

use I18n\Lib\Lang;
use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * BlogCategories cell
 */
class BlogCategoriesCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $Relation = TableRegistry::get( 'taxonomy_relationships');

    $Relation->belongsTo( 'Posts', [
      'className' => 'Blog.Posts',
      'foreignKey' => 'content_id',
    ]);

    $Relation->belongsTo( 'Categories', [
      'className' => 'Taxonomy.Categories',
      'foreignKey' => 'term_id',
    ]);

    $contents = $Relation->find()
      ->select([
        'total' => 'COUNT(*)',
        'Categories.id',
      ])
      ->contain([
        'Categories' => function($q){
          return $q->find( 'translations', [
            'locales' => Lang::current('iso3'),
          ]);
        },
        'Posts'
      ])
      ->where([
        'Posts.content_type' => 'Posts',
        'Posts.published' => true,
        'Posts.published_at <= NOW()'
      ])
      ->group([
        'taxonomy_relationships.term_id',
      ])
      ->order([
        'total' => 'desc'
      ])
      ->limit( $block->settings->limit);

    $categories = [];

    foreach( $contents as $category)
    {
      if ($category->category) {
        $category->category->set( 'count', $category->total);
        $category->category->set( 'title', $category->category->get('_translations')[Lang::current('iso3')]->title);
        $categories [] = $category->category;
      }
    }

    $this->set( compact( 'categories', 'block'));
  }
}
