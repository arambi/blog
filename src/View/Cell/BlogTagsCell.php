<?php
namespace Blog\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;

/**
 * BlogTags cell
 */
class BlogTagsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  { 
    $Relation = TableRegistry::get( 'taxonomy_relationships', [
    ]);

    $Relation->belongsTo( 'Posts', [
      'className' => 'Blog.Posts',
      'foreignKey' => 'content_id',
    ]);

    $Relation->belongsTo( 'Tags', [
      'className' => 'Taxonomy.Tags',
      'foreignKey' => 'term_id',
    ]);

    $contents = $Relation->find()
      ->select([
        'total' => 'COUNT(*)',
        'Tags.id',
        'Tags.title',
        'Tags.slug'
      ])
      ->contain([
        'Tags',
        'Posts'
      ])
      ->where([
        'Tags.locale' => Lang::current('iso3'),
        'Posts.content_type' => 'Posts',
        'Posts.published' => true,
        'Posts.published_at <= NOW()'
      ])
      ->group([
        'taxonomy_relationships.term_id',
      ])
      ->order([
        'total' => 'desc'
      ])
      ->limit( $block->settings->limit);
    
    $tags = [];

    foreach( $contents as $tag)
    {
      $tag->tag->set( 'count', $tag->total);
      $tags [] = $tag->tag;
    }
    $this->set( compact( 'tags', 'block'));
  }
}
