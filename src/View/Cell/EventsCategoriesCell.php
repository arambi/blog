<?php
namespace Blog\View\Cell;

use I18n\Lib\Lang;
use Cake\View\Cell;
use Cake\ORM\TableRegistry;
/**
 * EventsCategories cell
 */
class EventsCategoriesCell extends Cell
{

  public function display( $block)
  {
    $Relation = TableRegistry::get( 'taxonomy_relationships');

    $Relation->belongsTo( 'Events', [
      'className' => 'Blog.Events',
      'foreignKey' => 'content_id',
    ]);

    $Relation->belongsTo( 'Categories', [
      'className' => 'Taxonomy.Categories',
      'foreignKey' => 'term_id',
    ]);

    $contents = $Relation->find()
      ->select([
        'total' => 'COUNT(*)',
        'Categories.id',
      ])
      ->contain([
        'Categories' => function($q){
          return $q->find( 'translations', [
            'locales' => Lang::current('iso3'),
          ]);
        },
        'Events'
      ])
      ->where([
        'Events.content_type' => 'Events',
        'Events.published' => true,
        'Events.published_at >= NOW()'
      ])
      ->group([
        'taxonomy_relationships.term_id',
      ])
      ->order([
        'total' => 'desc'
      ])
      ->limit( $block->settings->limit);

    $categories = [];

    foreach( $contents as $category)
    {
      $category->category->set( 'count', $category->total);
      $category->category->set( 'title', $category->category->get('_translations')[Lang::current('iso3')]->title);
      $categories [] = $category->category;
    }
    $this->set( compact( 'categories', 'block'));
  }
}
