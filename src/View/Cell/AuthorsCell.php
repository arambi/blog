<?php
namespace Blog\View\Cell;

use Cake\View\Cell;
use Cake\Cache\Cache;

/**
 * Authors cell
 */
class AuthorsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $this->loadModel( 'Blog.Posts');

    $contents = Cache::read( 'Posts.Block.authors');

    if( !$contents)
    {
      $stmt = $this->Posts->connection()->execute(
        "SELECT p1 . author_id
          FROM contents p1
          INNER JOIN (
          SELECT max( published_at ) MaxPostDate, author_id
          FROM contents
          WHERE content_type = 'Posts' AND published = 1 AND published_at <= NOW()
          GROUP BY author_id
          ) p2 
          ON p1.author_id = p2.author_id
          AND p1.published_at = p2.MaxPostDate
          WHERE p1.content_type = 'Posts'  AND published = 1 AND published_at <= NOW()
          GROUP BY author_id
          ORDER BY p1.published_at DESC ");

      $rows = $stmt->fetchAll('assoc');

      $ids = collection( $rows)->extract( 'author_id')->toArray();

      $results = $this->Posts->Authors->find()
        ->where([
          'Authors.id IN' => $ids
        ])
        ->toArray();
      ;

      $results = collection( $results);
      foreach( $ids as $id)
      {
        $content = $results->firstMatch(['id' => $id]);

        if( $content)
        {
          $contents [] = $content;
        }
      }

      Cache::write( 'Posts.Block.authors', $contents);
    }

    $this->set( compact( 'block', 'contents'));
  }
}
