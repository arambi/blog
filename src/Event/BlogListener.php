<?php 

namespace Blog\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class BlogListener implements EventListenerInterface
{
  public function implementedEvents()
  {
    return [
      'Manager.View.Sites.beforeBuild' => 'websiteBuild',
    ];
  }

/**
 * Se encarga de modificar el crud de website
 * Le añade una pestaña nueva para meterle las preferencias de la tienda
 * 
 * @param  Cake\Event\event $event
 * @param  Table $table
 */
  public function websiteBuild( $event, $table)
  {
    $view = $event->subject();

    $table->crud->addFields([
      'settings.blog_with_commentaries' => [
        'label' => __d( 'admin', 'Con comentarios'),
        'type' => 'boolean'
      ],
      'settings.blog_commentaries_email_mandatory' => [
        'label' => __d( 'admin', 'Email obligatorio en comentarios'),
        'type' => 'boolean',
        'show' => 'content.settings.blog_with_commentaries'
      ],
      'settings.blog_without_subtitle' => [
        'label' => __d( 'admin', 'Sin subtítulo'),
        'type' => 'boolean'
      ],
      'settings.blog_without_summary2' => [
        'label' => __d( 'admin', 'Sin texto de portada'),
        'type' => 'boolean'
      ],
      'settings.blog_without_rss' => [
        'label' => __d( 'admin', 'Sin RSS'),
        'type' => 'boolean'
      ],
      'settings.blog_with_categories' => [
        'label' => __d( 'admin', 'Noticias con categorías'),
        'type' => 'boolean'
      ],
      'settings.blog_with_tags' => [
        'label' => __d( 'admin', 'Noticias con etiquetas'),
        'type' => 'boolean'
      ],
      'settings.blog_without_video' => [
        'label' => __d( 'admin', 'Noticias sin video'),
        'type' => 'boolean'
      ],
      'settings.blog_without_special' => [
        'label' => __d( 'admin', 'Sin noticia destacada'),
        'type' => 'boolean'
      ],
      'settings.events_with_categories' => [
        'label' => __d( 'admin', 'Eventos con categorías'),
        'type' => 'boolean'
      ],
      'settings.events_with_tags' => [
        'label' => __d( 'admin', 'Eventos con etiquetas'),
        'type' => 'boolean'
      ],
      'settings.blog_with_audios' => [
        'label' => __d( 'admin', 'Noticias con audios'),
        'type' => 'boolean'
      ],
      'settings.blog_with_photos' => [
        'label' => __d( 'admin', 'Con galería de fotos'),
        'type' => 'boolean'
      ],
      'settings.blog_with_authors' => [
        'label' => __d( 'admin', 'Con autores'),
        'type' => 'boolean'
      ],
      'settings.blog_with_docs' => [
        'label' => __d( 'admin', 'Con archivos'),
        'type' => 'boolean'
      ],
      'settings.blog_posts_paginate_limit' => [
        'label' => __d( 'admin', 'Número de noticias en blog'),
        'type' => 'numeric',
        'range' => [1, 20]
      ],
      'settings.blog_not_find_locales' => [
        'label' => __d( 'admin', 'No mostrar noticias que no estén traducidas'),
        'type' => 'boolean',
      ],
    ]);

    $view->addColumn( 'update', [
      'title' => __d( 'admin', 'Blog'),
      'box' => [
        [
          'title' => __d( 'admin', 'Características del blog'),
          'elements' => [
            'settings.blog_with_commentaries',
            'settings.blog_commentaries_email_mandatory',
            'settings.blog_with_categories',
            'settings.blog_with_tags',
            'settings.events_with_categories',
            'settings.events_with_tags',
            'settings.blog_without_subtitle',
            'settings.blog_without_summary2',
            'settings.blog_without_rss',
            'settings.blog_with_authors',
            'settings.blog_without_video',
            'settings.blog_without_special',
            'settings.blog_with_audios',
            'settings.blog_with_photos',
            'settings.blog_with_docs',
            'settings.blog_posts_paginate_limit',
            'settings.blog_not_find_locales',
          ]
        ],
      ]
    ]);
  }


} 